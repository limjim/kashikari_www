# unicorn.rb
worker_processes 2
timeout 1800
preload_app true

listen '/tmp/unicorn.kashikari.sock'
pid '/tmp/unicorn.kashikari.pid'

stderr_path 'log/unicorn_err.log'
stdout_path 'log/unicorn.log'

before_exec do |server|
  ENV['BUNDLE_GEMFILE'] = '/home/web/kashikari/current/Gemfile';
end

before_fork do |server, worker|
  old_pid = "#{ server.config[:pid] }.oldbin"
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      # 古いマスターをKill
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      # someone else did our job for us
    end
  end

  defined?(ActiveRecord::Base) and
      ActiveRecord::Base.connection.disconnect!
end

after_fork do |server, worker|
  defined?(ActiveRecord::Base) and
      ActiveRecord::Base.establish_connection

  #Rails.cache.reconnect
end