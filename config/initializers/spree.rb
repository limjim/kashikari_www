# Configure Spree Preferences
#
# Note: Initializing preferences available within the Admin will overwrite any changes that were made through the user interface when you restart.
#       If you would like users to be able to update a setting with the Admin it should NOT be set here.
#
# In order to initialize a setting do:
# config.setting_name = 'new value'
Spree.config do |config|
  config.currency = "JPY"
  config.display_currency = false
  config.default_country_id = 114
  config.searcher_class = Search
  config.allow_guest_checkout = false
  config.address_requires_state = false
  config.mails_from = "info@kashi-kari.jp"
  config.auto_capture = true
  config.products_per_page = 32
  config.check_for_spree_alerts = false
end

Spree.user_class = "Spree::User"

SpreeI18n::Config.available_locales = [:ja] # displayed on translation forms
SpreeI18n::Config.supported_locales = [:ja] # displayed on frontend select box