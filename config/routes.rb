Rails.application.routes.draw do

  Spree::Core::Engine.routes.draw do

    namespace :api, defaults: { format: 'json' } do
      resources :rental_items, only: [:update]
    end

    namespace :admin do
      get '/csv_imports', controller: :csv_imports, action: :index, as: :csv_import_index
      post '/csv_imports/template', controller: :csv_imports, action: :template, as: :csv_import_template, defaults: { format: 'csv' }
      post '/csv_imports/import', controller: :csv_imports, action: :import, as: :csv_import_import, defaults: { format: 'html' }

      resources :recurrings, except: :show do
        resources :plans, except: :show
      end

      resources :rental_items do
        member do
          put :ship
          put :return
          put :purchase
          get :export
        end
      end

      resources :taxons_descriptions, except: :show

      resources :subscriptions, only: :index
      resources :subscription_events, only: :index
    end

    resources :recurring_hooks, only: :none do
      post :handler, on: :collection
    end

    resources :subscriptions, only: [:index, :create, :destroy, :new]

    get '/subscriptions/step1' => 'subscriptions#step1', :as => :subscriptions_step1
    get '/subscriptions/step2' => 'subscriptions#step2', :as => :subscriptions_step2
    get '/subscriptions/registration' => 'subscriptions#registration', :as => :subscriptions_registration
    put '/subscriptions/registration' => 'subscriptions#update_registration', :as => :update_subscriptions_registration

    resources :rental_items

    post 'rental_items/cancel' => 'rental_items#cancel', :as => :rental_item_cancel

    #Filter Products
    #get '/products' => 'products#index'
    #get '/products/:name' => 'products#index'
    #get '/products/:brand_name/:item_name' => 'products#index'


    get 'brands' => 'brands#index', :as => :brands
    #get 'brands/:id' => 'brands#show', :as => :brand
    get 'brands/:id' => 'products#index', :as => :brand
    get 'brands/:brand_name/:item_name' => 'products#index'
    get 'brands/:id/*sub_id' => 'brands#show_with_category', :as => :brand_with_category
    #get 'category/:id' => 'category#show', :as => :category
    get 'category/:id' => 'products#index', :as => :category
    put 'users/add_to_favorite/:variant_id' => 'users#add_to_favorite', :as => :add_to_favorite_user

    # thanks
    get '/thankyou' => 'users#thankyou', :as => :thankyou
    get '/thanks_silver' => 'users#thanks_silver', :as => :thanks_silver
    get '/thanks_gold' => 'users#thanks_gold', :as => :thanks_gold
    get '/thanks_black' => 'users#thanks_black', :as => :thanks_black

    get 'account/billing' => 'users#billing', :as => :user_billing
    get 'account/history' => 'users#history', :as => :user_history
    get 'account/orders' => 'users#orders', :as => :user_orders
    get 'account/favorites' => 'users#favorites', :as => :user_favorites
    get 'account/setting' => 'users#setting', :as => :user_setting
    get 'account/unsubscribe' => 'users#unsubscribe', :as => :user_unsubscribe
    get 'account/edit/address' => 'users#edit_address', :as => :user_edit_address
    patch 'account/update_address' => 'users#update_address', :as => :user_update_address

    #checkout complete path
    get '/checkout/complete' => 'orders#complete', :as => :complete_order

    # pages
    get '/pages/qa' => 'pages#qa', :as => :pages_qa
    get '/pages/policy' => 'pages#policy', :as => :pages_policy
    get '/pages/termofuse' => 'pages#termofuse', :as => :pages_termofuse
    get '/pages/about' => 'pages#about', :as => :pages_about
    get '/pages/asct' => 'pages#asct', :as => :pages_asct
  end

  mount Spree::Core::Engine, :at => '/'

  #Error handling
  get  '*not_found' => 'application#routing_error'
  post '*not_found' => 'application#routing_error'

end
