# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.
#
# role :app, %w{deploy@example.com}
# role :web, %w{deploy@example.com}
# role :db,  %w{deploy@example.com}

set :branch, 'staging'

#role :app, %w{"10.0.0.108", "10.0.42.81"}
#role :web, %w{"10.0.0.108", "10.0.42.81"}
#role :db,  %w{"10.0.0.108", "10.0.42.81"}

# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

server "172.31.29.206", user: 'web', roles: %w{web app db}

# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult[net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start).
#
# Global options
# --------------
#  set :ssh_options, {
#    keys: %w(/home/rlisowski/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }
#
# And/or per server (overrides global)
# ------------------------------------
# server 'example.com',
#   user: 'user_name',
#   roles: %w{web app},
#   ssh_options: {
#     user: 'user_name', # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: 'please use keys'
#   }

set :ssh_options, {
    keys: [File.expand_path('~/.ssh/kashikari-key.pem')],
    forward_agent: true,
    auth_methods: %w(publickey)
}

before "deploy:assets:precompile","deploy:copy_database_yml"
namespace :deploy do
  task :copy_database_yml do
    on roles(:all) do |host|
      execute "cp #{release_path}/config/database.yml.stg #{release_path}/config/database.yml"
    end
  end
end