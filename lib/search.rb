class Search < Spree::Core::Search::Base
  # To change this template use File | Settings | File Templates.

  def get_base_scope
    base_scope = super
    base_scope = base_scope.in_taxons(taxon_ids) unless taxon_ids.blank?
    base_scope = base_scope.in_plan(plan) unless plan.blank?
    if order == "desc_by_favorite"
      base_scope = base_scope.desc_by_favorite()
    else
      base_scope = base_scope.descend_by_created_at()
    end
    base_scope
  end

  def prepare(params)
    super(params)

    #custom props
    #@properties[:parent_taxon] = params[:parent_taxon].blank? ? nil : Spree::Taxon.find(params[:parent_taxon])
    #@properties[:minimum_stock] = params[:minimum_stock].to_i
    #@properties[:order] = params[:order]

    taxon_ids = []
    taxon_ids.push params[:brand_id] if params[:brand_id].present?
    taxon_ids.push params[:category_id] if params[:category_id].present?
    taxon_ids.push params[:material_id] if params[:material_id].present?
    taxon_ids.push params[:design_id] if params[:design_id].present?
    taxon_ids.push params[:color_id] if params[:color_id].present?
    taxon_ids.push params[:size_id] if params[:size_id].present?

    @properties[:order] = params[:order]
    @properties[:plan] = params[:plan_id].blank? ? nil : Spree::Plan.find(params[:plan_id])
    @properties[:taxon_ids] = taxon_ids.blank? ? nil : taxon_ids.uniq
    #@properties[:parent_taxon_ids] = params[:parent_categories].blank? ? nil : params[:parent_categories].split(",").map!{|s| s.to_i}
  end

end