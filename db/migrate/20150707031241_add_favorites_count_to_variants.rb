class AddFavoritesCountToVariants < ActiveRecord::Migration
  def change
    add_column :spree_variants, :favorites_count, :integer, :null => false, :default => 0
  end
end
