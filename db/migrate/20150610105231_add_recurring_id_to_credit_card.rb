class AddRecurringIdToCreditCard < ActiveRecord::Migration
  def change
    add_reference :spree_credit_cards, :recurring, index: true
  end
end
