class AddSourceToSubscription < ActiveRecord::Migration
  def change
    add_column :spree_subscriptions, :source_id, :integer
    add_column :spree_subscriptions, :source_type, :string
    add_column :spree_subscriptions, :amount, :integer
  end
end
