class CreateTaxonsDescriptions < ActiveRecord::Migration
  def change
    create_table :spree_taxons_descriptions do |t|
    	 t.integer :taxon_1st_id ,references: :spree_taxons
    	 t.integer :taxon_2nd_id ,references: :spree_taxons
    	 t.string  :description
    	 t.datetime :deleted_at
    	 
    	 t.timestamps
    end
  end
end
