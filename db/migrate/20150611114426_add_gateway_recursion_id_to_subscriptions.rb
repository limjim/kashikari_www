class AddGatewayRecursionIdToSubscriptions < ActiveRecord::Migration
  def change
    add_column :spree_subscriptions, :gateway_recursion_id, :string
  end
end
