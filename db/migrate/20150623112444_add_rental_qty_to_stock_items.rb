class AddRentalQtyToStockItems < ActiveRecord::Migration
  def change
    add_column :spree_stock_items, :rental_qty, :integer, :default => 0
  end
end
