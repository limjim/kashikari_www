class AddPreferenceToRecurring < ActiveRecord::Migration
  def up
    add_column :spree_recurrings, :preferences, :text
  end

  def down
    remove_column :spree_recurrings, :preferences
  end
end
