class CreatePlanTaxons < ActiveRecord::Migration
  def change
    create_table :spree_plan_taxons do |t|
      t.references :plan
      t.references :taxon

      t.timestamps
    end
  end
end
