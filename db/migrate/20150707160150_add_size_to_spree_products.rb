class AddSizeToSpreeProducts < ActiveRecord::Migration
  def change
    add_column :spree_products, :size, :text
  end
end
