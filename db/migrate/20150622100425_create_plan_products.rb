class CreatePlanProducts < ActiveRecord::Migration
  def change
    create_table :spree_plan_products do |t|
      t.references :plan
      t.references :product

      t.timestamps
    end
  end
end
