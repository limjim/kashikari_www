class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :spree_subscriptions do |t|
      t.integer :plan_id
      t.string :email
      t.integer :user_id
      t.datetime :subscribed_at
      t.datetime :unsubscribed_at
    end
  end
end
