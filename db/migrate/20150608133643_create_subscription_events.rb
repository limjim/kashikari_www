class CreateSubscriptionEvents < ActiveRecord::Migration
  def change
    create_table :spree_subscription_events do |t|
      t.string :event_id
      t.integer :subscription_id
      t.string :request_type
      t.text :response
      t.datetime :created_at, :null => false
      t.datetime :updated_at, :null => false
    end
  end
end
