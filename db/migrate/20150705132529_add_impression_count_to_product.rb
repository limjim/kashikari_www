class AddImpressionCountToProduct < ActiveRecord::Migration
  def change
    add_column :spree_products, :impressions_count, :integer, :null => false, :default => 0
  end
end
