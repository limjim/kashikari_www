class AddAliasToTaxonomies < ActiveRecord::Migration
  def change
    add_column :spree_taxonomies, :alias, :string
    add_column :spree_taxons, :alias, :string
  end
end
