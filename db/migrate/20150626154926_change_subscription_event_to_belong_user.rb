class ChangeSubscriptionEventToBelongUser < ActiveRecord::Migration
  def change
    add_reference :spree_subscription_events, :user, index: true
  end
end
