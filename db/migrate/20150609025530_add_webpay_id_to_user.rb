class AddWebpayIdToUser < ActiveRecord::Migration
  def change
    add_column :spree_users, :webpay_customer_id, :string
  end
end
