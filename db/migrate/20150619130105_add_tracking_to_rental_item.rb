class AddTrackingToRentalItem < ActiveRecord::Migration
  def change
    add_column :spree_rental_items, :tracking, :string
  end
end
