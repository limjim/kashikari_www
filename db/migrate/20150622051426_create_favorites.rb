class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :spree_favorites do |t|
      t.references :user
      t.references :variant
      t.timestamps
    end
  end
end
