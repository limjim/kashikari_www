class CreatePlans < ActiveRecord::Migration
  def change
    create_table :spree_plans do |t|
      t.references :recurring
      t.string :name
      t.string :interval
      t.integer :interval_count, :default => 1
      t.integer :amount
      t.string :currency
      t.boolean :active, :default => false
      t.integer :available_item_amount, :default => 0
      t.boolean :default, :default => false
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
