class CreateRentalItems < ActiveRecord::Migration
  def change
    create_table :spree_rental_items do |t|
      t.references :user
      t.references :variant
      t.string :state
      t.datetime :delivered_at
      t.datetime :returned_at
      t.timestamps
    end
  end
end
