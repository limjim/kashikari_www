if Spree::ShippingMethod.all.to_a.length == 0

  begin
    japan = Spree::Zone.find_by_name!("Japan")
  rescue ActiveRecord::RecordNotFound
    japan = Spree::Zone.create!(name: "Japan", description: "Japan")
    ["Japan"].each do |name|
      japan.zone_members.create!(zoneable: Spree::Country.find_by!(name: name))
    end
  end

  default = Spree::ShippingCategory.find_or_create_by!(name: 'default')

  shipping_methods = [
      {
          :name => "default",
          :zones => [japan],
          :calculator => Spree::Calculator::Shipping::FlatRate.create!,
          :shipping_categories => [default]
      },
  ]

  shipping_methods.each do |shipping_method_attrs|
    Spree::ShippingMethod.create!(shipping_method_attrs)
  end

  {
      "default" => [0, "JPY"],
  }.each do |shipping_method_name, (price, currency)|
    shipping_method = Spree::ShippingMethod.find_by_name!(shipping_method_name)
    shipping_method.calculator.preferred_amount = price
    shipping_method.calculator.preferred_currency = currency
    shipping_method.save!
  end
end

