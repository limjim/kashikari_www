if Spree::Taxonomy.all.to_a.length == 0
  taxonomies = [
      {:name => "brand",
       :alias => "ブランド"},
      {:name => "item",
       :alias => "アイテム"},
      {:name => "color",
       :alias => "カラー"},
      {:name => "design",
       :alias => "デザイン"},
      {:name => "size",
       :alias => "サイズ"},
      {:name => "material",
       :alias => "素材"},
  ]

  taxonomies.each do |taxonomy_attrs|
    Spree::Taxonomy.create!(taxonomy_attrs)
  end

end

brand = Spree::Taxonomy.find_by!(name: "brand")
item = Spree::Taxonomy.find_by!(name: "item")
color = Spree::Taxonomy.find_by!(name: "color")
design = Spree::Taxonomy.find_by!(name: "design")
size = Spree::Taxonomy.find_by!(name: "size")
material = Spree::Taxonomy.find_by!(name: "material")

brand_names = ["23区","5351プール オム","H&M","P.S.FA","USJ","ベーセーストック","アーバン リサーチ","アーペーセー",
               "アールニューボールド","アオキ","アクアスキュータム","アザブテーラー","ア テストーニ","アトリエF&B",
               "アニエスベー","アバクロンビー＆フィッチ","アバハウス","アメリカンラグシー","アルマーニ コレツィオーニ",
               "アレキサンダー マックイーン","アンダーカバー","アンドエー","イヴサンローラン","イタルスタイル",
               "イッセイ・ミヤケ","インティメージ","ヴァレンティノ","ヴァンキッシュ","ヴィヴィアン ウエストウッド",
               "ウェッジウッド","ヴェルサーチ","エイプ","エービーエックス","エストネーション","エディフィス","エトロ",
               "エヌハリウッド","エミリオプッチ","エムシーエム","エルメス","エルメネジルド ゼニア","エンポリオアルマーニ",
               "オースチンリード","カナーリ","カリーナ","カルヴェン パリス","カルバンクライン","ギットマンブラザーズ",
               "グッチ","グラド","クリケット","クリスヴァンアッシュ","クリスチャン ディオール","クリスチャンラクロワ",
               "グローバル ワーク","クロエ","クロムハーツ","ケイタ マルヤマ","ケンゾー","コーチ","コシノヒロコ","コムサイズム",
               "コムサコレクション","コムサデモード","コムサメン","コムデギャルソン","コルネリアーニ","ザ・リバー","ザラ",
               "サルヴァトーレ フェラガモ","サンローラン","シーワード&スターン","ジェイクルー","ジェイプレス","シップス",
               "ジバンシー","ジムトンプソン","ジャーナル スタンダード","シャネル","シャルルジョルダン","ジャンニ ヴェルサーチ",
               "ジョージジェンセン","ジョルジオ アルマーニ","ジョンローレンスサリバン","スワロフスキー","セオリー","セラ","セリーヌ",
               "ソブリン","ダーバン","タイユアタイ","タケオキクチ","ダックス","ダナキャラン","ダブルタップス","ダンヒル","チャーチ",
               "ツモリチサト","ディースクエアード","ディオール オム","ティファニー","テットオム","デザインワークス","ドゥシャン",
               "トゥモローランド","トミー ヒルフィガー","トムフォード","トラサルディ","トランスコンチネンツ","トラディッショナル クラバット",
               "ドルチェ＆ガッバーナ","ドレイクス","D&G","ナッソー","ナノ ユニバース","ナンバーナイン","ニールバレッド","ニッキー",
               "ニノ パコリ","パーソンズ","バーニーズ ニューヨーク","バーバリー","バーバリー ブラックレーベル","バーバリーズ",
               "バーバリー ブローサム","バーバリー ロンドン","ハービーハドソン","ハイドロゲン","バグッタ","パトリックコックス",
               "バナナ リパブリック","バランタイン","パルジレリ","バレンシアガ","バレンチノ","ハロッズ","バンディベンディ",
               "ハンティングワールド","ビームス","ピエール・カルダン","ピエールバルマン","ビズモ","ヒューゴボス","ヒロコ コシノ",
               "ファソナブル","ファットーリ","フィオリオ","フェアファックス","フェンディ","プラダ","ブラックオンテットオム",
               "ブリオーニ","ブリオスタ","ブリック ハウス","ブルガリ","ブルックス ブラザーズ","フレッドペリー","ファックス",
               "ベドウィン","ベヴィラクア","ベネトン","ルシアン ペラフィネ","ベルサーチ","ベルルッティ","ボイコット","ポール スチュアート",
               "ポール スミス","ボッテガ ヴェネタ","ボリオリ","ポロ ラルフローレン","マーク ジェイコブス","マーク バイ ジェイコブス",
               "マリネッラ","マリオ ヴァレンティノ","マルゼン","ミキモト","ミッソーニ","ミラショーン","メッサジェリエ","メローラ",
               "メンズクラブ","メンズビギ","モスキーノ","ヤマモトカンサイ","ユナイテッド アローズ","ユニフォームエクスペリメント",
               "ヨウジヤマモト","ラブレス","ラルディーニ","ラルフローレン","ランセル","ランバン","リーガル","リーバイス",
               "リチャードジェイムス","ルイ ヴィトン","ルイジボレッリ","ルチアーノ ソプラー二","レノマ","ロアー","ロエベ","ロエン",
               "ロダ","ロッソ","ロッソ ビアンコ","ロバート フレイザー","ロベルト カヴァリ","ロンシャン","ワコマリア",
               "ノーブランド","ジュンメン"]

brand_permalinks = ["23ku","5351pour les hommes","h-and-m","psfa","usj","bc-stock","urban-research","apc",
                    "r-newbold","aoki","aquascutum","azabu-tailor","a-testoni","atelier-fb","agnesb","abercrombie",
                    "abahouse","americanragcie","armani-collezioni","alexandermcqueen","undercover","and-a",
                    "yves-saint-laurent","ital-style","isseimiyake","intimage","valentino","vanquish","viviennewestwood",
                    "wedgwood","versace","bape","abx","estnation","edifice","etro","n-hoolywood","emiliopucci","mcm","hermes",
                    "zegna","emporio-armani","austinreed","canali","carina","carven","calvinklein","gitman brothers","gucci",
                    "grado","cricket","krisvanassche","christian-dior","christian-lacroix","global work","chloe","chrome-hearts",
                    "keitamaruyama","kenzo","coach","koshinohiroko","commecaism","commecacollection","comme-ca-du-mode",
                    "comme-ca-men","momme-de-garcons","corneliani","the-river","zara","salvatore-ferragamo","saint-laurent",
                    "seaward-stearn","jcrew","jpress","ships","givenchy","jim-thompsons","journal-standard","chanel",
                    "charles-jourdan","gianni-versace","georgjensen","giorgio-armani","john-lawrence-sulivan","swarovski",
                    "theory","shera","celine","sovereign","durban","tie-your-tie","takeokikuchi","daks","donna-karan","wtaps",
                    "dunhill","church","tsumorichisato","dsquared2","diorhomme","tiffany","tetehomme","designworks","duchamp",
                    "tomorrowland","tommy","tomford","trussardi","transcontinents","traditional-cravat","dolce-gabbana",
                    "drakes","d-and-g","nassow","nanouniverse","nunber-nine","neil-barrett","nikki","nino pacoli","persons",
                    "barneys-newyork","burberry","burberry-blacklabel","burberrys","burberry-prorsum","burberry-london",
                    "harvie-hudson","hydrogen","bagutta","patrick-cox","bananarepublic","ballantyne","palzileri","balenciaga",
                    "valentino","harrods","bandy-bendy","huntingworld","beams","pierre-cardin","pierre-balmain","bizmo",
                    "hugo-boss","hirokokoshino","faconnable","fattori ","fiorio","fairfax","fendi","prada","black-on-tetehomme",
                    "brioni","briosta","brickhouse","bulgari","brooksbrothers","fredperry","fair fax","bedwin","bevilacqua",
                    "benetton","lucien-pellat-finet","versace","berluti","boycott","paulstuart","paulsmith","bottegaveneta",
                    "boglioli","polo-ralphlauren","marcjacobs","marc-by-marcjacobs","marinella","mario-valentino","maruzen",
                    "mikimoto","missoni","milaschon","messagerie","merola","mensclub","mens-bigi","moschino","kansaiyamamoto",
                    "united-arrows","uniformexperiment","yohjiyamamoto","lovless","lardini","ralphlauren","lancel","lanvin",
                    "regal","levis","richardjames","louisvuitton","luigiborrelli","luciano-soprani","renoma","roar","loewe",
                    "roen","roda","rosso","rosso-bianco","robert-fraser","robertocavalli","longchamp","wackomaria","no-brand",
                    "junmen"]

brand_names.each_with_index do |name, i|
  Spree::Taxon.find_or_create_by!(:name => name, :permalink => brand_permalinks[i], :parent_id => brand.root.id, :taxonomy => brand)
end

item_names = ["ネクタイ","カフス","タイピン","チーフ","ジレ","ペン"]
item_permalinks = ["necktie","cufflinks","tiepin","handkerchief","gilet","pen"]
item_names.each_with_index do |name, i|
  Spree::Taxon.find_or_create_by!(:name => name, :permalink => item_permalinks[i], :parent_id => item.root.id, :taxonomy => item)
end

color_names = ["ブルー","ネイビー","レッド","ブラック","ブラウン","ホワイト","パープル","グレー","ベージュ","ピンク","グリーン","カーキ","シルバー","イエロー","ボルドー","ガンメタリック","ゴールド","その他"]
color_permalinks = ["blue","navy","red","black","brown","white","purple","gray","beige","pink","green","khaki","silver","yellow","bordeaux","Ganmeta","gold","other-colors"]

color_names.each_with_index do |name, i|
  Spree::Taxon.find_or_create_by!(:name => name, :permalink => color_permalinks[i], :parent_id => color.root.id, :taxonomy => color)
end

material_names = [
    "シルク","カシミア","コットン","ウール","リネン","ナイロン","ポリエステル","キュプラ","レーヨン","テンセル","その他"]
material_permalinks = ["silk","cashmere","cotton","wool","linen","nylon","polyester","cupra","rayon","tencel","other-material"]

material_names.each_with_index do |name, i|
  Spree::Taxon.find_or_create_by!(:name => name, :permalink => material_permalinks[i], :parent_id => material.root.id, :taxonomy => material)
end


design_names = ["無地","小紋","ドッド","ストライプ","チェック","ペイズリー","その他"]
design_permalinks = ["plain","komon","dot","stripe","plaid","paisley","other-design"]
design_names.each_with_index do |name, i|
  Spree::Taxon.find_or_create_by!(:name => name, :permalink => design_permalinks[i], :parent_id => design.root.id, :taxonomy => design)
end

size_names = ["ナロー","レギュラー","ワイド"]
size_permalinks = ["narrow", "regular", "wide"]
size_names.each_with_index do |name, i|
  Spree::Taxon.find_or_create_by!(:name => name, :permalink => size_permalinks[i], :parent_id => size.root.id, :taxonomy => size)
end