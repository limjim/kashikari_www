if Spree::OptionType.all.to_a.length == 0
  Spree::OptionType.create!(:name => "Size", :presentation => "Size")

  option_type = Spree::OptionType.find_by_name!("Size")

  option_values = [
      {
          :name => "FREE",
          :presentation => "FREE",
          :option_type_id => option_type.id,
          :position => 1
      },
      {
          :name => "S",
          :presentation => "S",
          :option_type_id => option_type.id,
          :position => 2
      },
      {
          :name => "M",
          :presentation => "M",
          :option_type_id => option_type.id,
          :position => 3
      },
      {
          :name => "L",
          :presentation => "L",
          :option_type_id => option_type.id,
          :position => 4
      },

  ]

  option_values.each do |option_values_attrs|
    Spree::OptionValue.create!(option_values_attrs)
  end

end