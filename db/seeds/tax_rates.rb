if Spree::TaxRate.all.to_a.length == 0
japan = Spree::Zone.find_by_name!("Japan")
clothing = Spree::TaxCategory.find_by_name!("default")
tax_rate = Spree::TaxRate.create(
  :name => "Japan",
  :zone => japan,
  :amount => 0.08,
  :tax_category => clothing)
tax_rate.calculator = Spree::Calculator::DefaultTax.create!
tax_rate.save!
end

