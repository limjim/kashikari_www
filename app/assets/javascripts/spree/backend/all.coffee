#= require spree/backend
#= require spree/backend/rental_item

Spree.routes.rental_items_api = Spree.pathFor('api/rental_items')