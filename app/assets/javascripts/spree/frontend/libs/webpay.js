// Inspired by https://stripe.com/docs/stripe.js

mapCC = function (ccType) {
    if (ccType == 'MasterCard') {
        return 'mastercard';
    } else if (ccType == 'Visa') {
        return 'visa';
    } else if (ccType == 'American Express') {
        return 'amex';
    } else if (ccType == 'Discover') {
        return 'discover';
    } else if (ccType == 'Diners Club') {
        return 'dinersclub';
    } else if (ccType == 'JCB') {
        return 'jcb';
    }
}

$(document).ready(function () {
    // For errors that happen later.
    Spree.webpayPaymentMethod.prepend("<div id='webpayError' class='errorExplanation' style='display:none'></div>")

    if ($('#existing_cards').is('*')){
        Spree.webpayPaymentMethod.hide();
    }

    $('#use_existing_card_yes').on('click', function () {
        Spree.webpayPaymentMethod.hide();
        $('.existing-cc-radio').prop("disabled", false);
    });

    $('#use_existing_card_no').on('click', function () {
        Spree.webpayPaymentMethod.show();
        $('.existing-cc-radio').prop("disabled", true);
    });

    $(".cardNumber").payment('formatCardNumber');
    $(".cardExpiry").payment('formatCardExpiry');
    $(".cardCode").payment('formatCardCVC');

    $('[type="submit"]').on('click', function () {
        $('#webpayError').hide();
        if (Spree.webpayPaymentMethod.is(':visible')) {

            expiration = $('.cardExpiry:visible').payment('cardExpiryVal')
            params = {
                name: $("[name=\"subscription[source_attributes][name]\"]").val(),
                number: $('.cardNumber:visible').val().replace(/\s/g, ''),
                cvc: $('.cardCode:visible').val(),
                exp_month: expiration.month || 0,
                exp_year: expiration.year || 0
            };

            WebPay.createToken(params, webpayResponseHandler);
            return false;
        }
    });
});

webpayResponseHandler = function (status, response) {
    if (response.error) {
        if (response.error.code == 'card_declined') {
            $('#webpayError').html("カードが決済に失敗しました。発行会社にお問い合わせください。");
            $('#webpayError').show();
        } else if (response.error.code == 'incorrect_number'){
            $('#webpayError').html("カードの番号が不正です。入力したカードの番号が合っているか確認してください。");
            $('#webpayError').show();
        } else if (response.error.code == 'incorrect_cvc'){
            $('#webpayError').html("セキュリティコードが間違っています。入力したカードの番号が合っているか確認してください。");
            $('#webpayError').show();
        } else if (response.error.code == 'incorrect_expiry'){
            $('#webpayError').html("有効期限が間違っています。入力したカードの番号が合っているか確認してください。");
            $('#webpayError').show();
        } else if (response.error.code == 'processing_error'){
            $('#webpayError').html("処理中にエラーが発生しました。入力したカードの番号が合っているか確認してください。");
            $('#webpayError').show();
        } else {
            $('#webpayError').html("カードの決済に失敗しました。入力したカードの番号が合っているか確認してください。");
            $('#webpayError').show();
        }
    } else {
        Spree.webpayPaymentMethod.find('#card_number, #card_expiry, #card_code').prop("disabled", true);
        Spree.webpayPaymentMethod.find(".ccType").prop("disabled", false);
        Spree.webpayPaymentMethod.find(".ccType").val(mapCC(response.card.type));
        token = response['id'];

        // insert the token into the form so it gets submitted to the server
        gateway_payment_profile_id = response.id;
        last_digits= response.card.last4;
        month = response.card.exp_month;
        year = response.card.exp_year;

        Spree.webpayPaymentMethod.append("<input type='hidden' class='webpayToken' name='subscription[source_attributes][gateway_payment_profile_id]' value='" + gateway_payment_profile_id + "'/>");
        Spree.webpayPaymentMethod.append("<input type='hidden' class='webpayToken' name='subscription[source_attributes][last_digits]' value='" + last_digits + "'/>");
        Spree.webpayPaymentMethod.append("<input type='hidden' class='webpayToken' name='subscription[source_attributes][month]' value='" + month + "'/>");
        Spree.webpayPaymentMethod.append("<input type='hidden' class='webpayToken' name='subscription[source_attributes][year]' value='" + year + "'/>");
        Spree.webpayPaymentMethod.append("<input type='hidden' class='webpayToken' name='subscription[card_token]' value='" + token + "'/>");
        Spree.webpayPaymentMethod.parents("form").get(0).submit();
    }
}

