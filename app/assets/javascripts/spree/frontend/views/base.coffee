$ ->
  # SP Product Detail Cordinate Style
  if $('body').hasClass('sp') && $('.detail_thumb').length
    liHeight = $('.detail_thumb').find('li').width()
    $('.detail_thumb').find('li').height(liHeight)
    $('.detail_thumb').find('.middle').css('top',-liHeight)
    $('.detail_thumb').find('.base').css('top',-liHeight*2)

  # Flash
  height = $('#flash').height()
  setTimeout (->
    $('#flash').slideUp('slow')
  ), 5000
