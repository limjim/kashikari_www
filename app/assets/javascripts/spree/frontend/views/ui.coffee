$ ->
  pagetop = $('.btt')
  pagetop.click ->
    $('body, html').animate { scrollTop: 0 }, 300
    return false

# bxSlider
$ ->
  $(".thumb_bxslider").bxSlider
    mode: 'fade'
    pagerCustom: '.thumb_bxpager'
    infiniteLoop: true
    prevText: ''
    nextText: ''

  $('.pager_bxslider').bxSlider
    infiniteLoop: false
    pager: false
    prevText: ''
    nextText: ''

  $(".fav_bxslider").bxSlider
    minSlides: 3
    maxSlides: 3
    slideWidth: 160
    slideMargin: 10
    pager: false
    prevText: ''
    nextText: ''

  $(".past_bxslider").bxSlider
    minSlides: 4
    maxSlides: 4
    slideWidth: 115
    slideMargin: 10
    pager: false
    prevText: ''
    nextText: ''

  $(".plan_selector").bxSlider
    infiniteLoop: false
    hideControlOnEnd: true
    pager: false
    prevText: ''
    nextText: ''
    startSlide: 1

  width = screen.width - 20
  $(".referal_bxslider").bxSlider
    minSlides: 3
    maxSlides: 3
    slideWidth: width
    slideMargin: 10
    prevText: ''
    nextText: ''

  # 変数宣言
  body = document.body
  # activeNav = undefined
  togglePush = $('.open_pushmenu')

  # マスクを定義
  mask = document.createElement('div')
  mask.className = 'mask'

  # Push Menu
  $('.push_buttoms').click ->
    # Push menu
    togglePush.click ->
      $('body').addClass 'pm-open'
      document.body.appendChild mask
      # activeNav = 'push-menu'
      return
    # Mask click function
    mask.addEventListener 'click', ->
      $('body').removeClass 'pm-open'
      document.body.removeChild mask
      # activeNav = ''
      return

  # Filter Accrdion
  $('.brand_accordion').click ->
    brandList = $(this).parent('.acrd_box').parent('.filter_brand').prev('div')
    if brandList.css('display') == 'none'
      brandList.slideDown()
      $(this).text('リストを閉じる')
    else
      brandList.slideUp()
      $(this).text('ブランドを選択する')

  # Push Menu Accordion
  $('.toggle_accordion').click ->
    if $(this).next('ul').css('display') == 'none'
      $(this).next('ul').slideDown()
    else
      $(this).next('ul').slideUp()

 

# お気に入り
$(document).on 'ajax:success', 'a.vote', (status,data,xhr)->
  # toggle links
  $("a.vote[data-id=#{data.id}]").each ->
    $a = $(this)
    href = $a.attr 'href'
    text = $a.text()
    $a.text($a.data('toggle-text')).attr 'href', $a.data('toggle-href')
    $a.data('toggle-text', text).data 'toggle-href', href
    if $(this).hasClass('liked')
      $(this).removeClass('liked')
    else
      $(this).addClass('liked')
    return

  return
