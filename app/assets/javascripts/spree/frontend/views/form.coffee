$ ->
  $("#validateForm").validate()

  # レンタル時のモーダル
  $('.rental_item').click ->

    # モーダルコンテンツのIDを取得
    modal = '#' + $(this).attr('data-target')

    itemImage = $(this).parents('li').find('.item-image').attr('src')
    itemBrand = $(this).parents('li').find('.item-brand').text()
    itemCategory = $(this).parents('li').find('.item-category').text()
    rentalForm = $(this).parents('li').find('.rental-form').html()
    rentalCancelForm = $(this).parents('li').find('.rental-form-cancel').html()

    $(modal).find('.modal-image').attr('src', itemImage)
    $(modal).find('.modal-brand').text(itemBrand)
    $(modal).find('.modal-brand').append(itemCategory)
    $(modal).find('.modal-submit').empty().append(rentalForm)
    $(modal).find('.modal-submit-cancel').empty().append(rentalCancelForm)

    # オーバーレイ用の要素を追加
    # モーダルコンテンツの表示位置を設定する関数

    modalResize = ->
      # ウィンドウの横幅、高さを取得
      w = $(window).width()
      h = $(window).height()
      # モーダルコンテンツの表示位置を取得
      x = (w - $(modal).outerWidth(true)) / 2
      y = (h - $(modal).outerHeight(true)) / 2
      # モーダルコンテンツの表示位置を設定
      $(modal).css
        'left': x + 'px'
        'top': y + 'px'
      return

    $(this).after('<div class="modal-overlay"></div>')

    # オーバーレイをフェードイン
    $('.modal-overlay').fadeIn 'slow'
    # モーダルコンテンツの表示位置を設定
    modalResize()
    # モーダルコンテンツフェードイン
    $(modal).fadeIn 'slow'

    # 「.modal-overlay」あるいは「.modal-close」をクリック
    $('.modal-overlay, .modal-close').off().click ->
      # モーダルコンテンツとオーバーレイをフェードアウト
      $(modal).fadeOut 'slow'
      $('.modal-overlay').fadeOut 'slow', ->
        # オーバーレイを削除
        $('.modal-overlay').remove()
        return
      return

    # リサイズしたら表示位置を再取得
    $(window).on 'resize', ->
      modalResize()
      return

    return false

  # Subscription Selector
  $('.sbsc').click ->
    if $(this).hasClass('sbsc1')
      sbsNum = 1
    else if $(this).hasClass('sbsc2')
      sbsNum = 2
    else if $(this).hasClass('sbsc3')
      sbsNum = 3
    else if $(this).hasClass('sbsc4')
      sbsNum = 4
    $('#plan_id').val(sbsNum)
    $('.type_list').children('ul').children('li').removeClass('selected')
    $('.type_list').children('ul').children('li').children('form').hide()
    $(this).parent('li').addClass('selected')
    $(this).parent('li').children('form').show()

  $('.use_billing').click ->
    if $(this).is(':checked')
      $('.shipping_form').slideUp()
    else
      $('.shipping_form').slideDown()

  # unsubscribe confirm
  $(".sbscConfirmBtn").click ->
    if !$('.sbscConfirmCkb').is(':checked')
      alert('登録するには上記項目内容を確認し、チェックボックスにチェックをしてください')
      return false

  # unsubscribe confirm
  $(".unSbscConfirmBtn").click ->
    if !$('.unSbscConfirmCkb').is(':checked')
      alert('解約をするには上記項目内容を確認し、チェックボックスにチェックをしてください')
      return false

  # Disable submit with enter key
  $('input').keypress (e) ->
    if /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
      code = if e.keyCode then e.keyCode else e.which
      if code == 13 or code == 10
        jQuery(this).blur()
        return false
      return

  # 住所自動入力
  $('#subscription_address_attributes_zipcode').jpostal({
    postcode : ['#subscription_address_attributes_zipcode'],
    address : {
      '#subscription_address_attributes_city' : '%3', # 都道府県
      '#subscription_address_attributes_address1' : '%4%5', # 市町村と町域
    }
  })