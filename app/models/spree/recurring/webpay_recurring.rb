module Spree
  class Recurring < Spree::Base
    class WebpayRecurring < Recurring
      include ApiHandler

      #WEBHOOKS = ['recursion.succeeded', 'recursion.failed', 'recursion.created', 'recursion.resumed', 'recursion.deleted', 'charge.succeeded', 'charge.failed', 'charge.refunded', 'charge.captured', 'customer.created', 'customer.updated', 'customer.deleted']
      WEBHOOKS = ['recursion.succeeded', 'recursion.failed', 'charge.succeeded', 'charge.failed']


      INTERVAL = { month: 'Monthly', year: 'Annually' }
      CURRENCY = { usd: 'USD', gbp: 'GBP', jpy: 'JPY', eur: 'EUR', aud: 'AUD', hkd: 'HKD', sek: 'SEK', nok: 'NOK', dkk: 'DKK', pen: 'PEN', cad: 'CAD'}

      def payment_source_class
        CreditCard
      end
    end
  end
end