module Spree
  class Subscription < ActiveRecord::Base
    #include RoleSubscriber
    include RestrictiveDestroyer
    include ApiHandler

    acts_as_restrictive_destroyer column: :unsubscribed_at
    attr_accessor :card_token, :source_attributes, :existing_card, :address_attributes

    belongs_to :plan
    belongs_to :user

    belongs_to :source, polymorphic: true

    has_many :events, class_name: 'Spree::SubscriptionEvent'

    validates :plan_id, :email, :user_id, presence: true
    validates :plan_id, uniqueness: {scope: [:user_id, :unsubscribed_at]}
    #validates :user_id, uniqueness: {scope: :unsubscribed_at}

    delegate_belongs_to :plan, :api_plan_id

    after_initialize :build_source
    before_validation :set_email, on: :create
    before_validation :set_amount, on: :create

    validate :verify_plan, on: :create

    def build_source
      return unless new_record?

      if existing_card.present? && source.blank? && plan.recurring.try(:payment_source_class)
        credit_card = CreditCard.find(existing_card)
        if credit_card.user_id != self.user_id || credit_card.user_id.blank?
          raise Core::GatewayError.new Spree.t(:invalid_credit_card)
        end
        self.source = credit_card
      elsif source_attributes.present? && source.blank? && plan.recurring.try(:payment_source_class)

        self.source = plan.recurring.payment_source_class.new(source_attributes)
        self.source.recurring_id = plan.recurring.id
        self.source.payment_method = Spree::PaymentMethod.find_by(:type => "Spree::PaymentMethod::Webpay")
        self.source.user_id = self.user_id
      end
    end

    private

    def set_email
      self.email = user.try(:email)
    end

    def set_amount
      self.amount = plan.amount
    end

    def verify_plan
      errors.add :plan_id, "is not active." unless plan.try(:visible?)
    end
  end
end