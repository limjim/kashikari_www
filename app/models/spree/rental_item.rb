require 'csv'
require 'nkf'

module Spree
  class RentalItem < ActiveRecord::Base
    include RestrictiveDestroyer

    acts_as_restrictive_destroyer column: :returned_at

    belongs_to :user
    belongs_to :variant
    has_many :state_changes, as: :stateful

    state_machine initial: :ready, use_transactions: false do

      event :ship do
        transition from: [:ready], to: :shipped
      end
      before_transition to: :shipped, do: :before_ship

      event :return do
        transition from: [:shipped], to: :returned
      end
      before_transition to: :returned, do: :before_return

      event :purchase do
        transition from: [:shipped, :ready], to: :purchased
      end
      before_transition to: :purchased, do: :before_purchase

      event :cancel do
        transition from: [:ready], to: :cancel
      end

      after_transition do |rental, transition|
        rental.state_changes.create!(
            previous_state: transition.from,
            next_state:     transition.to,
            name:           'rental_item',
        )
      end
    end

    def variant
      Spree::Variant.unscoped { super }
    end

    def user
      Spree::User.unscoped { super }
    end


    def export
      orders_export = CSV.generate do |csv|
        csv << %w(レンタルID レンタル日 レンタルSKU 名前 電話番号 郵便番号 住所1 住所2)

        csv_line = []
        csv_line << self.id
        csv_line << self.created_at.in_time_zone("Asia/Tokyo").strftime('%Y%m%d')
        csv_line << self.variant.sku
        csv_line << "#{self.user.ship_address.last_name} #{self.user.ship_address.first_name}"
        csv_line << "#{self.user.ship_address.phone.to_s}"
        csv_line << "#{self.user.ship_address.zipcode}"
        csv_line << self.user.ship_address.city + self.user.ship_address.address1
        csv_line << self.user.ship_address.address2
        csv << csv_line
      end
      file = NKF.nkf('-Ws -Lw', orders_export)
    end

    def self.to_csv list
      orders_export = CSV.generate do |csv|
        csv << %w(レンタルID レンタル日 レンタルSKU レンタルステータス ユーザID 姓 名 電話番号 郵便番号 住所1 住所2)
        list.each do |rental_item|
          csv_line = []
          csv_line << rental_item.id
          csv_line << rental_item.created_at.in_time_zone("Asia/Tokyo").strftime('%Y%m%d')
          csv_line << rental_item.variant.sku
          csv_line << rental_item.state
          csv_line << rental_item.user.id
          csv_line << rental_item.user.ship_address.last_name
          csv_line << rental_item.user.ship_address.first_name
          csv_line << "#{rental_item.user.ship_address.phone.to_s}"
          csv_line << "#{rental_item.user.ship_address.zipcode}"
          csv_line << rental_item.user.ship_address.city + rental_item.user.ship_address.address1
          csv_line << rental_item.user.ship_address.address2
          csv << csv_line
        end
      end

      file = NKF.nkf('-Ws -Lw', orders_export)
    end

    private

    def before_ship
      self.delivered_at =  Time.current
    end

    def before_return
      variant.stock_items.first.adjust_rental_qty(-1)
      self.returned_at =  Time.current
    end

    def before_purchase
      variant.stock_items.first.adjust_rental_qty(-1)
    end

  end
end

