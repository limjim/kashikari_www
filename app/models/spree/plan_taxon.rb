module Spree
  class PlanTaxon < ActiveRecord::Base
    belongs_to :plan
    belongs_to :taxon
  end
end
