module Spree
  Order.class_eval do

    def available_payment_methods

      @available_payment_methods ||= (PaymentMethod.available(:front_end) + PaymentMethod.available(:both)).uniq

      # Check if there's any shipped rental items. In that case COD payment is unavailable.
      if self.user.present? && self.user.rental_items.any?
        if (self.line_items.pluck(:variant_id) & user.rental_items.where(state: :shipped).pluck(:variant_id)).any?
          # TODO fix hardcoding
          @available_payment_methods.delete(Spree::PaymentMethod.find_by(name: "代金引換"))
        end
      end

      @available_payment_methods
    end

    private

    def after_cancel
      shipments.each { |shipment| shipment.cancel! }
      #payments.completed.each { |payment| payment.cancel! }
      #send_cancel_email

      credit = Adjustment.new(amount: total.abs * -1, label: Spree.t(:rma_credit))
      credit.source = self
      credit.adjustable = self
      credit.save

      self.update!
    end

  end
end
