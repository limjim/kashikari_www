Spree::CreditCard.class_eval do
  belongs_to :recurring, :class_name => 'Spree::Recurring'
  has_many :subscriptions, as: :source
end