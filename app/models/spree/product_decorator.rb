module Spree
  Product.class_eval do
    is_impressionable :counter_cache => true, :unique => :all

    has_many :image
    has_many :variant
    has_many :plan_products
    has_many :plans, through: :plan_products
    has_many :favorites, through: :variants
    has_many :rental_items, through: :variants_including_master

    def self.created_at_scopes
      [
          :ascend_by_created_at,
          :descend_by_created_at,
      ]
    end

    add_simple_scopes created_at_scopes

    scope :in_taxons, lambda { |ids|
      id = arel_table[:id]
      joins(:taxons).where("spree_products_taxons.taxon_id" => ids).group(id).having(id.count.eq(ids.size))
    }

    scope :in_plan, lambda { |plan|
      joins(:plans).where("spree_plans.id" => plan.id)
    }

    scope :desc_by_favorite, lambda {
      group("spree_variants.product_id").order("SUM(spree_variants.favorites_count) DESC")
    }

    #constant
    CATEGORIES = {
        brand: 1,
        item: 2,
        color: 3,
        design: 4,
        size: 5,
        material: 6,
    }.freeze

    def brands
      taxons.where(parent_id: CATEGORIES[:brand])
    end

    def items
      taxons.where(parent_id: CATEGORIES[:item])
    end

    def colors
      taxons.where(parent_id: CATEGORIES[:color])
    end

    def designs
      taxons.where(parent_id: CATEGORIES[:design])
    end

    def sizes
      taxons.where(parent_id: CATEGORIES[:size])
    end

    def materials
      taxons.where(parent_id: CATEGORIES[:material])
    end

    def total_favorite
      master.favorites_count
    end

    def has_plan?(plan)
      plans.include?(plan)
    end

  end
end
