Spree::User.class_eval do
  has_many :subscriptions
  has_many :rental_items
  has_many :favorites
  has_many :subscription_events

  after_create  :send_welcome

  def liked?(variant)
    favorites.pluck(:variant_id).include?(variant.id)
  end

  def send_welcome
    Spree::SubscriptionMailer.welcome_email(self.id).deliver
  end

  def find_or_create_webpay_customer(token=nil)
    return api_customer if webpay_customer_id?
    customer = if token
                 Webpay::Customer.create(description: email, email: email, card: token)
               else
                 Webpay::Customer.create(description: email, email: email)
               end
    update_column(:webpay_customer_id, customer.id)
    customer
  end

  def api_customer
    Webpay::Customer.retrieve(webpay_customer_id)
  end
end