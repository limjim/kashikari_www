Spree::Variant.class_eval do
  has_many :rental_items
  has_many :favorites

  def rental_qty
    stock_items.sum(:rental_qty)
  end

end