require 'sanitize'
module Spree
  class TaxonsDescription < ActiveRecord::Base
  	include RestrictiveDestroyer

  	acts_as_restrictive_destroyer

  	belongs_to :taxon_1st, :class_name => Spree::Taxon, :foreign_key => "taxon_1st_id"
  	belongs_to :taxon_2nd, :class_name => Spree::Taxon, :foreign_key => "taxon_2nd_id"
  	validates :description, presence: true
    validates :taxon_1st_id, :presence => { :message => "ブランド名を選択してください。" }
    validates :taxon_2nd_id, :presence => { :message => "アイテム名を選択してください。" }
    #validates :description, :presence => { :message => "説明文を入力してください。" }
  	validates_length_of :description, :maximum => 20, :message => "20文字以内で入力してください"
  	validates_uniqueness_of :taxon_1st_id, scope: :taxon_2nd_id, conditions: -> { where(deleted_at: nil) } ,:message => "既にその組み合わせで説明文は登録されています"

  	scope :active, -> { undeleted.where(active: true) }
  	before_save :remove_html_tag
  	private

  	def remove_html_tag
  		self.description =  ActionController::Base.helpers.strip_tags(self.description)
  	end

  end
end