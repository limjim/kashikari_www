module Spree
  Taxon.class_eval do

    has_many :plan_taxons
    has_many :plans, through: :plan_taxons

    def set_permalink

      if parent.present? && taxonomy.root != parent
        self.permalink = [parent.permalink, (permalink.blank? ? name.to_url : permalink.split('/').last)].join('/')
      else
        self.permalink = name.to_url if permalink.blank?
      end
    end

  end
end
