module Spree
  class Favorite < ActiveRecord::Base
    belongs_to :user
    belongs_to :variant, :counter_cache => :favorites_count

    def variant
      Spree::Variant.unscoped { super }
    end

    def user
      Spree::User.unscoped { super }
    end

  end
end
