module Spree
  Image.class_eval do
    #before_validation :parse_attachment_from_source
    attr_accessor :source

    attachment_definitions[:attachment][:styles] = {
        :mini => '80x80>', # thumbs under image
        :small => '160x160>', # images on category view
        :thumb => '240x240>', # images on category view
        :product => '400x400>', # full product image
        :large => '600x600>' # light box image
    }

    attachment_definitions[:attachment][:path] = Kashikari::Application.config.image_path
    attachment_definitions[:attachment][:default_url] = Kashikari::Application.config.default_url

    Spree::Image.attachment_definitions[:attachment][:convert_options] = { all: Kashikari::Application.config.paperclip_options }


    def parse_attachment_from_source
      if source.present?
        self.attachment = URI.parse(source)
      end
    end

  end
end