module Spree
  class PlanProduct < ActiveRecord::Base
    belongs_to :plan
    belongs_to :product
  end
end
