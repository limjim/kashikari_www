require 'csv'
require 'nkf'
require 'net/http'
require 'open-uri'

module Spree
  class CsvImport

    class CsvError < StandardError
      attr_reader :status, :data
      def initialize(message)
        super(message)
      end
    end


    def persisted?
      false
    end

    class << self

      def generate_template(fields)
        ((required_fields + fields).uniq & available_fields).join(',')
      end

      def import(file)
        result = 0

        open(file.path, 'r:cp932:utf-8', undef: :replace) do |f|
          csv = CSV.new(f, :headers => :first_row)

          csv.each_with_index do |row, i|

            Spree::Product.transaction do
              attrs = row.to_hash.symbolize_keys

              raise "SKUが指定されていません: #{i}行目" if attrs[:sku].blank?
              raise "NAMEが指定されていません: #{i}行目" if attrs[:name].blank?
              raise "PRICEが指定されていません: SKU: #{attrs[:sku]}" if attrs[:price].blank?
              raise "BRANDが指定されていません: SKU: #{attrs[:sku]}" if attrs[:brand].blank?
              raise "ITEMが指定されていません: SKU: #{attrs[:sku]}" if attrs[:category].blank?

              begin

              Rails.logger.info("CSV import: start importing #{attrs[:sku]}")

              if Spree::Product.where(:name => attrs[:name]).blank?

                shipping_category = Spree::ShippingCategory.first

                product_attrs = {
                    :name => attrs[:name],
                    :shipping_category => shipping_category,
                    :price => attrs[:price],
                    :description => attrs[:description],
                    :sku => attrs[:sku],
                    :available_on => Time.zone.now,
                    :size => attrs[:size]
                }

                Spree::Config[:currency] = "JPY"

                product = Spree::Product.create!(product_attrs)
                Rails.logger.info("CSV import: new product is created #{attrs[:sku]}")

              else
                product = Spree::Product.find_by!(:name => attrs[:name])
                product.name = attrs[:name]
                product.price = attrs[:price]
                product.description = attrs[:description]
                product.sku = attrs[:sku]
                product.size = attrs[:size]
                Rails.logger.info("CSV import: product will be updated #{attrs[:sku]}")
              end

              # create category and associate
              taxons = []

              begin
              brand = attrs[:brand]
              brand_taxonomy = Spree::Taxonomy.find_by(name: "brand")
              taxons << Spree::Taxon.find_by!(:name => brand, :taxonomy_id => brand_taxonomy.id, :parent_id => brand_taxonomy.root.id)

              rescue Exception => e
                raise CsvError.new("#{e.message} カラム名:brand")
              end

              if attrs[:color].present?
              begin
              color_list = attrs[:color].split(" ")
              color_list.each do |name|
                taxonomy = Spree::Taxonomy.find_by(name: "color")
                taxons << Spree::Taxon.find_by!(:name => name, :taxonomy_id => taxonomy.id, :parent_id => taxonomy.root.id)
              end
              rescue Exception => e
                raise CsvError.new("#{e.message} カラム名:color")
              end
              end

              if attrs[:category].present?
              begin
              category_list = attrs[:category].split(" ")
              category_list.each do |name|
                taxonomy = Spree::Taxonomy.find_by(name: "item")
                taxons << Spree::Taxon.find_by!(:name => name, :taxonomy_id => taxonomy.id, :parent_id => taxonomy.root.id)
              end
              rescue Exception => e
                raise CsvError.new("#{e.message} カラム名:category")
              end
              end

              if attrs[:design].present?
              begin
              design_list = attrs[:design].split(" ")
              design_list.each do |name|
                taxonomy = Spree::Taxonomy.find_by(name: "design")
                taxons << Spree::Taxon.find_by!(:name => name, :taxonomy_id => taxonomy.id, :parent_id => taxonomy.root.id)
              end
              rescue Exception => e
                raise CsvError.new("#{e.message} カラム名:design")
              end
              end

              if attrs[:necktie_size].present?
              begin
              necktie_size_list = attrs[:necktie_size].split(" ")
              necktie_size_list.each do |name|
                taxonomy = Spree::Taxonomy.find_by(name: "size")
                taxons << Spree::Taxon.find_by!(:name => name, :taxonomy_id => taxonomy.id, :parent_id => taxonomy.root.id)
              end
              rescue Exception => e
                raise CsvError.new("#{e.message} カラム名:necktie_size")
              end
              end

              if attrs[:material].present?
              begin
              material_list = attrs[:material].split(" ")
              material_list.each do |name|
                taxonomy = Spree::Taxonomy.find_by(name: "material")
                taxons << Spree::Taxon.find_by!(:name => name, :taxonomy_id => taxonomy.id, :parent_id => taxonomy.root.id)
              end
              rescue Exception => e
                raise CsvError.new("#{e.message} カラム名:material")
              end
              end

              if product.taxons.any?
                taxons.each do |taxon|
                  if !product.taxons.exists?(id: taxon.id)
                    product.taxons << taxon
                  end
                end
              else
                product.taxons << taxons
              end

              plans = []
              plan_list = attrs[:plan].split(" ")
              plan_list.each do |name|
                plans << Spree::Plan.find_by!(name: name)
              end

              if product.plans.any?
                plans.each do |plan|
                  if !product.plans.exists?(id: plan.id)
                    product.plans << plan
                  end
                end
              else
                product.plans << plans
              end

              if product.master.images.present?
                Rails.logger.info("CSV import: destroy images for update #{attrs[:sku]}")

                product.master.images.each do |image|
                  image.destroy
                end
              end

              image_host = attrs[:image_host]
              image_files = attrs[:image_file].split(" ")
              image_files.each do |image|
                target = "http://#{image_host}#{image}"

                url = URI.parse(target)
                req = Net::HTTP.new(url.host, url.port)
                path = url.path if url.path.present?
                res = req.request_head(path || '/')

                if res.code != "404" && res.header.content_type.include?("image")
                 product.master.images.create!(:attachment => url)
                else
                  raise CsvError.new("画像が見つかりません カラム名:image_file ファイル名:#{image}")
                end
              end

              if product.total_on_hand < 1
                variant = product.master
                stock_location = Spree::StockLocation.first
                stock_movement = stock_location.stock_movements.build(:quantity => 1)
                stock_movement.stock_item = stock_location.set_up_stock_item(variant)
                stock_movement.save!
                Rails.logger.error("CSV import: stock is added #{attrs[:sku]}")
              end

              product.save!
              Rails.logger.info("CSV import: imported successfully #{attrs[:sku]}")
              result = result + 1

              rescue Exception => e
                Rails.logger.error("CSV import: failed cause:#{e.message} SKU: #{attrs[:sku]}")
                raise CsvError.new("CSVインポートが失敗しました。原因:#{e.message} SKU: #{attrs[:sku]}")
              end

            end
          end
        end

        return result
      end

      def required_fields
        %w(product_sku variant_sku brand name japanese_name price taxon zones gender)
      end

      def optional_fields
        %w(description japanese_description available_on available_till shipping_start delivery_period material japanese_material remarks japanese_remarks meta_keywords japanese_meta_keywords meta_description japanese_meta_description on_hand size shoulder length height width pit\ to\ pit sleeve sleeve\ from\ neck neck waist front\ rise inseam heel circumference brim gusset handle thickness handle\ width top\ width bicep hem sleeve\ opening hip waist\ height thigh back\ rise knee leg\ opening arm\ opening)
      end

      def available_fields
        required_fields + optional_fields
      end

    end

  end
end