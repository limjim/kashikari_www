module Spree
  class Recurring < Spree::Base
    class WebpayRecurring < Spree::Recurring
      module ApiHandler
        module SubscriptionEventApiHandler
          def retrieve_event(event_id)
            begin
              client.event.retrieve(event_id)
            rescue error_class => e
              logger.error "Webpay Event error: #{e.message}"
              errors.add :base, "Webpay Event error: #{e.message}"
              nil
            end
          end

          def client
             @client ||= WebPay.new(preferred_secret_key)
          end
        end
      end
    end
  end
end