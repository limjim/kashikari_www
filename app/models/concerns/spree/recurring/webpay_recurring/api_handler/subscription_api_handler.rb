module Spree
  class Recurring < Spree::Base
    class WebpayRecurring < Spree::Recurring
      module ApiHandler
        module SubscriptionApiHandler
          def subscribe(subscription)
            raise_invalid_object_error(subscription, Spree::Subscription)

            customer_id = find_or_create_customer(subscription)

            recursion = client.recursion.create(
                amount: subscription.plan.amount,
                currency: "jpy",
                customer: customer_id,
                period: "month",
                description: subscription.plan.name,
            )

            if recursion.status != "active"
              raise WebPay::ApiError.new("カードの決済に失敗しました。入力したカードの番号が合っているか確認してください。")
              return
            end

            recursion
          end

          def change_subscription(subscription, old_subscription)
            raise_invalid_object_error(subscription, Spree::Subscription)

            if subscription.amount > old_subscription.amount
              upgrade(subscription, old_subscription)
            else
              downgrade(subscription, old_subscription)
            end

          end

          def upgrade(subscription, old_subscription)
            customer_id = find_or_create_customer(subscription)
            old_recursion = client.recursion.retrieve(old_subscription.gateway_recursion_id)
            if old_recursion.deleted == true
              raise WebPay::ApiError.new("該当の定期課金は既に削除されています。サービス運営会社にお問い合わせください。")
              return
            end

            extra_charge = subscription.amount - old_subscription.amount
            client.charge.create(
                amount: extra_charge,
                currency: "jpy",
                customer: customer_id,
                description: "会員プラン変更料 #{old_subscription.plan.name}から#{subscription.plan.name}に変更"
            )

            new_recursion = client.recursion.create(
                amount: subscription.plan.amount,
                currency: "jpy",
                customer: customer_id,
                period: "month",
                first_scheduled: old_recursion.next_scheduled,
                description: subscription.plan.name,
            )


            if new_recursion.status != "active"
              raise WebPay::ApiError.new("カードの決済に失敗しました。入力したカードの番号が合っているか確認してください。")
              return
            end

            new_recursion

          end

          def downgrade(subscription, old_subscription)
            customer_id = find_or_create_customer(subscription)

            old_recursion = client.recursion.retrieve(old_subscription.gateway_recursion_id)
            if old_recursion.deleted == true
              raise WebPay::ApiError.new("該当の定期課金は既に削除されています。サービス運営会社にお問い合わせください。")
              return
            end

            new_recursion = client.recursion.create(
                amount: subscription.plan.amount,
                currency: "jpy",
                customer: customer_id,
                period: "month",
                first_scheduled: old_recursion.next_scheduled,
                description: subscription.plan.name,
            )


            if new_recursion.status != "active"
              raise WebPay::ApiError.new("カードの決済に失敗しました。入力したカードの番号が合っているか確認してください。")
              return
            end

            new_recursion


          end

          def unsubscribe(subscription)
            raise_invalid_object_error(subscription, Spree::Subscription)
            client.recursion.delete(id: subscription.gateway_recursion_id)
          end

          private

          def find_or_create_customer(subscription)
            if subscription.source.gateway_customer_profile_id.present?
              subscription.source.gateway_customer_profile_id
            else
              customer = client.customer.create(
                  email: subscription.user.email,
                  description: subscription.user.email,
                  card: subscription.source.gateway_payment_profile_id,
              )

              subscription.source.update_attributes!({
                                                         gateway_customer_profile_id: customer.id,
                                                         gateway_payment_profile_id: nil
                                                     })

              customer.id
            end
          end
        end
      end
    end
  end
end