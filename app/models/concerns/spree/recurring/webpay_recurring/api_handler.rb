module Spree
  class Recurring < Spree::Base
    class WebpayRecurring < Spree::Recurring
      module ApiHandler
        extend ActiveSupport::Concern

        included do
          include SubscriptionApiHandler
          include SubscriptionEventApiHandler
        end

        def client
          @client ||= WebPay.new(preferred_secret_key)
        end

        def error_class
          WebPay::ApiError
        end

        def raise_invalid_object_error(object, type)
          raise error_class.new("Not a valid object.") unless object.is_a?(type)
        end
      end
    end
  end
end