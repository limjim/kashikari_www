class ApplicationController < ActionController::Base
  include Jpmobile::ViewSelector
  protect_from_forgery with: :exception
  before_action :load_taxons 
  before_action :find_taxons

  def load_taxons
    @brands = Spree::Taxonomy.where(name: "brand").includes(root: :children)
    @categories = Spree::Taxonomy.where(name: "item").includes(root: :children)
    @colors = Spree::Taxonomy.where(name: "color").includes(root: :children)
    @designs = Spree::Taxonomy.where(name: "design").includes(root: :children)
    @sizes = Spree::Taxonomy.where(name: "size").includes(root: :children)
    @materials = Spree::Taxonomy.where(name: "material").includes(root: :children)
    @plans = Spree::Plan.visible
    #@taxonomies = Spree::Taxonomy.where.not(name: ["brand", "item"]).includes(root: :children)
  end

  def find_taxons
    brand = Spree::Taxonomy.find_by_name("brand")
    @brand_taxons = Spree::Taxon.where(parent_id: brand.id)
    @h_brands = Hash.new
    @brand_taxons.each { |i|
      count = Spree::ProductsTaxon.where(taxon_id: i.id).to_a.count
      @h_brands[i.name] = count
    }

    @a_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ア%'", brand.id)
    @i_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'イ%'", brand.id)
    @u_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'ウ%' OR spree_taxons.name LIKE 'ヴ%')", brand.id)
    @e_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'エ%'", brand.id)
    @o_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'オ%'", brand.id)

    @ka_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'カ%' OR spree_taxons.name LIKE 'ガ%')", brand.id)
    @ki_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'キ%' OR spree_taxons.name LIKE 'ギ%')", brand.id)
    @ku_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'ク%' OR spree_taxons.name LIKE 'グ%')", brand.id)
    @ke_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'ケ%' OR spree_taxons.name LIKE 'ゲ%')", brand.id)
    @ko_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'コ%' OR spree_taxons.name LIKE 'ゴ%')", brand.id)

    @sha_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'サ%' OR spree_taxons.name LIKE 'ザ%')", brand.id)
    @shi_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'シ%' OR spree_taxons.name LIKE 'ジ%')", brand.id)
    @shu_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'ス%' OR spree_taxons.name LIKE 'ズ%')", brand.id)
    @she_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'セ%' OR spree_taxons.name LIKE 'ゼ%')", brand.id)
    @sho_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'ソ%' OR spree_taxons.name LIKE 'ゾ%')", brand.id)

    @ta_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'タ%' OR spree_taxons.name LIKE 'ダ%')", brand.id)
    @tsi_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'チ%' OR spree_taxons.name LIKE 'ヂ%')", brand.id)
    @tsu_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'ツ%' OR spree_taxons.name LIKE 'ヅ%')", brand.id)
    @te_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'テ%' OR spree_taxons.name LIKE 'デ%')", brand.id)
    @to_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'ト%' OR spree_taxons.name LIKE 'ド%')", brand.id)

    @na_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ナ%'", brand.id)
    @ni_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ニ%'", brand.id)
    @nu_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ヌ%'", brand.id)
    @ne_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ネ%'", brand.id)
    @no_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ノ%'", brand.id)

    @ha_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'ハ%' OR spree_taxons.name LIKE 'バ%' OR spree_taxons.name LIKE 'パ%')", brand.id)
    @hi_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'ヒ%' OR spree_taxons.name LIKE 'ビ%' OR spree_taxons.name LIKE 'ピ%')", brand.id)
    @fu_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'フ%' OR spree_taxons.name LIKE 'ブ%' OR spree_taxons.name LIKE 'プ%')", brand.id)
    @he_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'ヘ%' OR spree_taxons.name LIKE 'ベ%' OR spree_taxons.name LIKE 'ペ%')", brand.id)
    @ho_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND (spree_taxons.name LIKE 'ホ%' OR spree_taxons.name LIKE 'ボ%' OR spree_taxons.name LIKE 'ポ%')", brand.id)

    @ma_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'マ%'", brand.id)
    @mi_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ミ%'", brand.id)
    @mu_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ム%'", brand.id)
    @me_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'メ%'", brand.id)
    @mo_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'モ%'", brand.id)

    @ya_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ヤ%'", brand.id)
    @yu_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ユ%'", brand.id)
    @yo_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ヨ%'", brand.id)

    @ra_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ラ%'", brand.id)
    @ri_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'リ%'", brand.id)
    @ru_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ル%'", brand.id)
    @re_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'レ%'", brand.id)
    @ro_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ロ%'", brand.id)

    @wa_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ワ%'", brand.id)
    @tso_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ヲ%'", brand.id)
    @um_brands = Spree::Taxon.where("spree_taxons.parent_id = ? AND spree_taxons.name LIKE 'ン%'", brand.id)

    brand_ka_ids = Array.new
    if !@a_brands.empty?
      @a_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@i_brands.empty?
      @i_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@u_brands.empty?
      @u_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@e_brands.empty?
      @e_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@o_brands.empty?
      @o_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end

    if !@ka_brands.empty?
      @ka_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@ki_brands.empty?
      @ki_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@ku_brands.empty?
      @ku_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@ke_brands.empty?
      @ke_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@ko_brands.empty?
      @ko_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end

    if !@sha_brands.empty?
      @sha_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@shi_brands.empty?
      @shi_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@shu_brands.empty?
      @shu_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@she_brands.empty?
      @she_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@sho_brands.empty?
      @sho_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end

    if !@ta_brands.empty?
      @ta_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@tsi_brands.empty?
      @tsi_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@tsu_brands.empty?
      @tsu_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@te_brands.empty?
      @te_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@to_brands.empty?
      @to_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end

    if !@na_brands.empty?
      @na_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@ni_brands.empty?
      @ni_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@nu_brands.empty?
      @nu_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@ne_brands.empty?
      @ne_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@no_brands.empty?
      @no_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end

    if !@ha_brands.empty?
      @ha_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@hi_brands.empty?
      @hi_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@fu_brands.empty?
      @fu_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@he_brands.empty?
      @he_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@ho_brands.empty?
      @ho_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end

    if !@ma_brands.empty?
      @ma_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@mi_brands.empty?
      @mi_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@mu_brands.empty?
      @mu_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@me_brands.empty?
      @me_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@mo_brands.empty?
      @mo_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end

    if !@ya_brands.empty?
      @ya_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@yu_brands.empty?
      @yu_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@yo_brands.empty?
      @yo_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end

    if !@ra_brands.empty?
      @ra_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@ri_brands.empty?
      @ri_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@ru_brands.empty?
      @ru_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@re_brands.empty?
      @re_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@ro_brands.empty?
      @ro_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end


    if !@wa_brands.empty?
      @wa_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@tso_brands.empty?
      @tso_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if !@um_brands.empty?
      @um_brands.each{ |i|
        brand_ka_ids.push(i.id)
      }
    end
    if brand_ka_ids.empty?
      @other_brands = Spree::Taxon.where("spree_taxons.parent_id = (?)", brand.id)
    else
      @other_brands = Spree::Taxon.where("spree_taxons.id NOT IN (?) AND spree_taxons.parent_id = (?)", brand_ka_ids, brand.id)
    end
    item = Spree::Taxonomy.find_by_name("item")
    @item_taxons = Spree::Taxon.where(parent_id: item.id)
    @h_items = Hash.new
    @item_taxons.each { |i|
      count = Spree::ProductsTaxon.where(taxon_id: i.id).to_a.count
      @h_items[i.name] = count
    }


    color = Spree::Taxonomy.find_by_name("color")
    @color_taxons = Spree::Taxon.where(parent_id: color.id)
    @h_colors = Hash.new
    @color_taxons.each { |i|
      count = Spree::ProductsTaxon.where(taxon_id: i.id).to_a.count
      @h_colors[i.name] = count
    }


    design = Spree::Taxonomy.find_by_name("design")
    @design_taxons = Spree::Taxon.where(parent_id: design.id)
    @h_designs = Hash.new
    @design_taxons.each { |i|
      count = Spree::ProductsTaxon.where(taxon_id: i.id).to_a.count
      @h_designs[i.name] = count
    }

    size = Spree::Taxonomy.find_by_name("size")
    @size_taxons = Spree::Taxon.where(parent_id: size.id)
    @h_sizes = Hash.new
    @size_taxons.each { |i|
      count = Spree::ProductsTaxon.where(taxon_id: i.id).to_a.count
      @h_sizes[i.name] = count
    }

    material = Spree::Taxonomy.find_by_name("material")
    @material_taxons = Spree::Taxon.where(parent_id: material.id)
    @h_materials = Hash.new
    @material_taxons.each { |i|
      count = Spree::ProductsTaxon.where(taxon_id: i.id).to_a.count
      @h_materials[i.name] = count
    }


    @plans = Spree::Plan.visible
    @h_plans = Hash.new
    @plans.each { |i|
      count = Spree::PlanProduct.group(:product_id).having("min(spree_plan_products.plan_id) IN (?)", i.id).to_a.count
      @h_plans[i.name] = count
    }
  end
  # Execption Handling
  if Kashikari::Application.config.allow_error_page
    rescue_from Exception,                        with: :render_500
    rescue_from ActiveRecord::RecordNotFound,     with: :render_404
    rescue_from ActionController::RoutingError,   with: :render_404
  end

  #
  def routing_error
    raise ActionController::RoutingError.new(params[:path])
  end

  def render_404(e = nil)
    logger.info "Rendering 404 with exception: #{e.message}" if e

    if request.xhr?
      render json: { error: '404 error' }, status: 404
    else
      format = params[:format] == :json ? :json : :html
      render template: 'spree/errors/error_404', formats: format, status: 404, layout: false, content_type: 'text/html'
    end
  end

  def render_500(e = nil)
    logger.info "Rendering 500 with exception: #{e.message}" if e

    if request.xhr?
      render json: { error: '500 error' }, status: 500
    else
      format = params[:format] == :json ? :json : :html
      render template: 'spree/errors/error_500', formats: format, status: 500, layout: false, content_type: 'text/html'
    end
  end

  def after_sign_in_path_for(resource_or_scope)
    stored_location_for(resource_or_scope) || products_path
  end

end
