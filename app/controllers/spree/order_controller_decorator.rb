module Spree
  OrdersController.class_eval do

    def complete
      @order = Order.find_by_number!(params[:id])
      if !@order.completed?
        redirect_to root_path and return
      end
    end

  end
end
