module Spree
  class RentalItemsController < StoreController
   prepend_before_filter :load_object

   def create
      # validate user has an active plan
      if !@user.subscriptions.undeleted.any?
        redirect_to account_path, notice: '有効な月額プランがありません'
        return
      end

      # validate user has enough rental amount
      if @user.rental_items.any?
        if @user.rental_items.where(state: [:ready, :shipped]).count >= @user.subscriptions.undeleted.first.plan.available_item_amount
          redirect_to account_path, notice: 'レンタル出来る上限数を超えています'
          return
        end
      end

      @rental_item = RentalItem.new(rental_item_params)

      # validate the plan includes item
      if !@rental_item.variant.product.has_plan?(@user.subscriptions.undeleted.first.plan)
        redirect_to account_path, notice: '現在のプランではレンタル出来ない商品です'
        return
      end

      # validate item has enough stock
      if !Stock::Quantifier.new(@rental_item.variant).can_supply? (@rental_item.variant.rental_qty + 1)
        redirect_to account_path, notice: '在庫が足りません'
        return
      end

      # increment rental item stock

      @rental_item.variant.stock_items.first.adjust_rental_qty(1)

      @rental_item.user_id = @user.id

      if @rental_item.save
        redirect_to account_path, notice: 'レンタルに成功しました'
      else
        redirect_to account_path
      end
    end

    def cancel
     renta_id = params['rental_item']['id']
     @reta_item = Spree::RentalItem.find(renta_id)
     @reta_item.cancel!
     flash[:success] = Spree.t(:cancel)
     #rentalMailer.rental_email(@reta_item).deliver
     redirect_to account_path
   end

   private

   def load_object
    @user ||= spree_current_user
      #authorize! params[:action].to_sym, @user
    end

    def rental_item_params
      params.require(:rental_item).permit(:variant_id)
    end

  end
end