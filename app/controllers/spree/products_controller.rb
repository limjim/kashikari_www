module Spree
  class ProductsController < Spree::StoreController
    before_action :load_product, only: :show
    before_action :load_taxon, only: :index
    before_action :find_taxons, only: [:index, :show]
    impressionist :actions => [:show]

    rescue_from ActiveRecord::RecordNotFound, :with => :render_404
    helper 'spree/taxons'

    respond_to :html

    def index
      count = 0
      # brand = Spree::Taxonomy.find_by_name("brand")
      # @brand_taxons = Spree::Taxon.where(parent_id: brand.id)
      # @h_brands = Hash.new
      
      unless params['id'].nil?
        check_404 = Taxon.find_by_permalink!(params[:id])
        taxon_name = Spree::Taxon.where("spree_taxons.permalink = ?", params['id']).to_a()
        check_404 = Taxon.find_by_id!(taxon_name.first['parent_id'])
        parent_name = Spree::Taxon.where("spree_taxons.id = ?", taxon_name.first['parent_id']).to_a()
        type_name = parent_name.first['name']
        if type_name.to_s == 'brand'
          params['brand_name'] = params['id']
        else
          params['item_name'] = params['id']
        end
      end
      unless params['brand_name'].nil?
        count += 1
        arr_brands = params['brand_name'].split(',')
      end

      unless params['item_name'].nil?
        count += 1
        arr_items = params['item_name'].split(',')
      end

      unless params['color_id'].nil?
        count += 1
        arr_colors = params['color_id'].split(',')
      end

      unless params['design_id'].nil?
        count += 1
        arr_designs = params['design_id'].split(',')
      end

      unless params['size_id'].nil?
        count += 1
        arr_sizes = params['size_id'].split(',')
      end

      unless params['material_id'].nil?
        count += 1
        arr_materials = params['material_id'].split(',')
      end

      unless params['plan_id'].nil?
        count += 1
        arr_plans = params['plan_id'].split(',')
      end

      taxon_brands = Spree::Taxon.where("spree_taxons.permalink IN (?)", arr_brands).to_a().collect{ |p| p.id }
      taxon_items = Spree::Taxon.where("spree_taxons.permalink IN (?)", arr_items).to_a().collect{ |p| p.id }

      @pro_plans = PlanProduct.group(:product_id).having("min(spree_plan_products.plan_id) IN (?)", arr_plans).to_a.collect {|p| p.product_id}
      @pro_brands = ProductsTaxon.where(taxon_id: taxon_brands).to_a.collect {|p| p.product_id}
      @pro_items = ProductsTaxon.where(taxon_id: taxon_items).to_a.collect {|p| p.product_id}
      @pro_colors = ProductsTaxon.where(taxon_id: arr_colors).to_a.collect {|p| p.product_id}
      @pro_designs = ProductsTaxon.where(taxon_id: arr_designs).to_a.collect {|p| p.product_id}
      @pro_sizes = ProductsTaxon.where(taxon_id: arr_sizes).to_a.collect {|p| p.product_id}
      @pro_materials = ProductsTaxon.where(taxon_id: arr_materials).to_a.collect {|p| p.product_id}
      
      pro_array = Array.new

      if !@pro_plans.empty?
        pro_array.push(@pro_plans)
      end

      if !@pro_brands.empty?
        pro_array.push(@pro_brands)
      end

      if !@pro_items.empty?
        pro_array.push(@pro_items)
      end

      if !@pro_colors.empty?
        pro_array.push(@pro_colors)
      end

      if !@pro_designs.empty?
        pro_array.push(@pro_designs)
      end

      if !@pro_sizes.empty?
        pro_array.push(@pro_sizes)
      end

      if !@pro_materials.empty?
        pro_array.push(@pro_materials)
      end

      if count == 0
        if params[:order] == 'desc_by_created_at'
          @products = Spree::Product.includes(product_includes).all.order('spree_products.id DESC').page(params[:page])
        elsif params[:order] == 'asc_by_price'
          @products = Spree::Product.includes(product_includes).all.order('spree_prices.amount ASC').page(params[:page])
        elsif params[:order] == 'desc_by_price'
          @products = Spree::Product.includes(product_includes).all.order('spree_prices.amount DESC ').page(params[:page])
        elsif params[:order] == 'desc_by_favorite'
          # if !spree_current_user.nil?
          #   variant_ids = Spree::Favorite.where(:user_id => spree_current_user.id).to_a.collect {|f| f.variant_id}
          #   pro_ids_f = Spree::Variant.where(:id => variant_ids).to_a.collect {|v| v.product_id}
          #   products_f = Spree::Product.includes(product_includes).where(:id => pro_ids_f)
          #   products_n_f = Spree::Product.includes(product_includes).where('spree_products.id NOT IN (?)', pro_ids_f)

          #   products = Array.new

          #   if !products_f.empty?
          #     products_f.each { |p|
          #       products.push(p)
          #     }
          #   end

          #   if !products_n_f.empty?
          #     products_n_f.each { |p|
          #       products.push(p)
          #     }
          #   end

          #   if variant_ids.empty?
          #     @products = Spree::Product.includes(product_includes).all.order('spree_products.id DESC').page(params[:page])
          #   else
          #     @products = Kaminari.paginate_array(products).page(params[:page])
          #   end

            
          # else
          #   @products = Spree::Product.includes(product_includes).all.order('spree_products.id DESC').page(params[:page])
          # end
          sql = "SELECT * FROM `spree_products` LEFT OUTER JOIN `spree_variants` ON `spree_variants`.`product_id` = `spree_products`.`id` AND `spree_variants`.`is_master` = 1 AND `spree_variants`.`deleted_at` IS NULL LEFT OUTER JOIN `spree_assets` ON `spree_assets`.`viewable_id` = `spree_variants`.`id` AND `spree_assets`.`type` IN ('Spree::Image') AND `spree_assets`.`viewable_type` = 'Spree::Variant' LEFT OUTER JOIN `spree_prices` ON `spree_prices`.`variant_id` = `spree_variants`.`id` AND `spree_prices`.`currency` = 'JPY' AND `spree_prices`.`deleted_at` IS NULL WHERE `spree_products`.`deleted_at` IS NULL GROUP BY spree_variants.product_id  ORDER BY SUM(spree_variants.favorites_count) DESC"
          @products = Kaminari.paginate_array(Spree::Product.find_by_sql(sql)).page(params[:page])
        elsif params[:order] == 'desc_by_rental'
          products = Array.new
          products_all = Spree::Product.includes(product_includes).all.order('spree_products.id DESC')
          #Loop 3 times to get righ order
          if !products_all.empty?
            products_all.each { |p|
              if p.total_on_hand > p.master.rental_qty
                products.push(p)
              end
            }
            products_all.each { |p|
              if (p.total_on_hand <= p.master.rental_qty && p.total_on_hand >= 1)
                products.push(p)
              end
            }
            products_all.each { |p|
              if p.total_on_hand < 1
                products.push(p)
              end
            }
            @products = Kaminari.paginate_array(products).page(params[:page])
          else
            @products = Spree::Product.includes(product_includes).all.order('spree_products.id DESC').page(params[:page])
          end
        elsif params[:order] == 'asc_by_rental'
          products = Array.new
          products_all = Spree::Product.includes(product_includes).all.order('spree_products.id DESC')
          #Loop 3 times to get righ order
          if !products_all.empty?
            products_all.each { |p|
              if p.total_on_hand < 1
                products.push(p)
              end
            }
            products_all.each { |p|
              if (p.total_on_hand <= p.master.rental_qty && p.total_on_hand >= 1)
                products.push(p)
              end
            }
            products_all.each { |p|
              if p.total_on_hand > p.master.rental_qty
                products.push(p)
              end
            }
            @products = Kaminari.paginate_array(products).page(params[:page])
          else
            @products = Spree::Product.includes(product_includes).all.order('spree_products.id DESC').page(params[:page])
            item_all = Spree::Product.includes(product_includes).all.order('spree_products.id DESC').to_a
            @brand_taxons.each { |i|
              brand_all = Spree::ProductsTaxon.where(taxon_id: i.id).to_a
              @h_brands[i.name] = (item_all & brand_all).count
            }
          end
        else
          @products = Spree::Product.includes(product_includes).all.order('spree_products.id DESC').page(params[:page])
        end

      else
        if pro_array.count < 2
          if count < 2
            pro_ids = pro_array
          else
            pro_ids = nil
          end
        else
          pro_ids = pro_array.reduce{ |x,y| x & y }
        end

        if params[:order] == 'desc_by_created_at'
          @products = Spree::Product.includes(product_includes).where(id: pro_ids).order('spree_products.id DESC').page(params[:page])
        elsif params[:order] == 'asc_by_price'
          @products = Spree::Product.includes(product_includes).where(id: pro_ids).order('spree_prices.amount ASC').page(params[:page])
        elsif params[:order] == 'desc_by_price'
          @products = Spree::Product.includes(product_includes).where(id: pro_ids).order('spree_prices.amount DESC').page(params[:page])
        elsif params[:order] == 'desc_by_favorite'

        #   if !spree_current_user.nil?
        #     if pro_ids.nil?
        #      @products = Spree::Product.includes(product_includes).where(id: 0).page(params[:page]).to_a
        #    else
        #     products_fil = Spree::Product.includes(product_includes).where(:id => pro_ids)
        #     variant_ids = Spree::Favorite.where(:user_id => spree_current_user.id).to_a.collect {|f| f.variant_id}
        #     pro_ids_f = Spree::Variant.where(:id => variant_ids).to_a.collect {|v| v.product_id}
        #     products_f = Spree::Product.includes(product_includes).where(:id => pro_ids_f)

        #     product_new_f = Array.new
        #     products_f.each{ |p|
        #       if products_fil.include? p
        #         product_new_f.push(p)
        #       end
        #     }

        #     product_not_f = Array.new
        #     products_fil.each{ |p|
        #       if !product_new_f.include? p
        #         product_not_f.push(p)
        #       end
        #     }

        #     if variant_ids.empty?
        #       products = products_fil
        #     else
        #       products = product_new_f.concat(product_not_f)
        #     end

        #     @products = Kaminari.paginate_array(products).page(params[:page])
        #   end
        # else
        #   @products = Spree::Product.includes(product_includes).where(id: pro_ids).order('spree_products.id DESC').page(params[:page])
        # end
        if pro_ids.nil?
           str_pr = 0
        else
          if !pro_ids.kind_of?(Array)
            str_pr = pro_ids[0]
          else
            str_pr = pro_ids.join(',')
          end
        end
        sql = "SELECT  DISTINCT `spree_products`.`id` FROM `spree_products` LEFT OUTER JOIN `spree_variants` ON `spree_variants`.`product_id` = `spree_products`.`id` AND `spree_variants`.`is_master` = 1 AND `spree_variants`.`deleted_at` IS NULL LEFT OUTER JOIN `spree_assets` ON `spree_assets`.`viewable_id` = `spree_variants`.`id` AND `spree_assets`.`type` IN ('Spree::Image') AND `spree_assets`.`viewable_type` = 'Spree::Variant' LEFT OUTER JOIN `spree_prices` ON `spree_prices`.`variant_id` = `spree_variants`.`id` AND `spree_prices`.`currency` = 'JPY' AND `spree_prices`.`deleted_at` IS NULL WHERE `spree_products`.`deleted_at` IS NULL AND `spree_products`.`id` IN (#{str_pr}) GROUP BY spree_variants.product_id  ORDER BY SUM(spree_variants.favorites_count) DESC"
        @products = Kaminari.paginate_array(Spree::Product.find_by_sql(sql)).page(params[:page])
        # item_all = Kaminari.paginate_array(Spree::Product.find_by_sql(sql)).to_a
        # @brand_taxons.each { |i|
        #   brand_all = Spree::ProductsTaxon.where(taxon_id: i.id).to_a
        #   @h_brands[i.name] = (item_all & brand_all).count
        # }
      elsif params[:order] == 'desc_by_rental'        
        if pro_ids.nil?
              @products = Spree::Product.includes(product_includes).where(id: 0).page(params[:page]).to_a
        else
            products = Array.new
            products_all = Spree::Product.includes(product_includes).where(id: pro_ids).order('spree_products.id DESC')
            #Loop 3 times to get righ order
            products_all.each { |p|
              if p.total_on_hand > p.master.rental_qty
                products.push(p)
              end
            }
            products_all.each { |p|
              if (p.total_on_hand <= p.master.rental_qty && p.total_on_hand >= 1)
                products.push(p)
              end
            }
            products_all.each { |p|
              if p.total_on_hand < 1
                products.push(p)
              end
            }
            @products = Kaminari.paginate_array(products).page(params[:page])
        end
      elsif params[:order] == 'asc_by_rental'
        if pro_ids.nil?
              @products = Spree::Product.includes(product_includes).where(id: 0).page(params[:page]).to_a
        else
            products = Array.new
            products_all = Spree::Product.includes(product_includes).where(id: pro_ids).order('spree_products.id DESC')
            #Loop 3 times to get righ order
            products_all.each { |p|
              if p.total_on_hand < 1
                products.push(p)
              end
            }
            products_all.each { |p|
              if (p.total_on_hand <= p.master.rental_qty && p.total_on_hand >= 1)
                products.push(p)
              end
            }
            products_all.each { |p|
              if p.total_on_hand > p.master.rental_qty
                products.push(p)
              end
            }
            @products = Kaminari.paginate_array(products).page(params[:page])
        end
      else
        @products = Spree::Product.includes(product_includes).where(id: pro_ids).order('spree_products.id DESC').page(params[:page])
      end
    end   

    if !params['brand_name'].nil? && !params['item_name'].nil? &&  params['color_id'].nil? && params['design_id'].nil? && params['size_id'].nil? && params['material_id'].nil?
        #@descriptions = TaxonsDescription.where("taxon_1st_id = ? AND taxon_2nd_id = ?", arr_brands, arr_items)
        @descriptions = TaxonsDescription.where(taxon_1st_id: taxon_brands, taxon_2nd_id: taxon_items)
      end
    end

    def show
      @variants = @product.variants_including_master.active(current_currency).includes([:option_values, :images])
      @product_properties = @product.product_properties.includes(:property)
      @taxon = Spree::Taxon.find(params[:taxon_id]) if params[:taxon_id]
      impressionist(@product)

      color = @product.taxons.where(taxonomy: Spree::Taxonomy.find_by(name: "color")).first
      item = @product.taxons.where(taxonomy: Spree::Taxonomy.find_by(name: "item")).first

      @searcher = build_searcher(params.merge(color_id: color.present? ? color.id : "", category_id: item.present? ? item.id : "", include_images: true))
      @related_items = @searcher.retrieve_products.limit(8)

      if @user
        @rent_items = @user.rental_items.variants
      end
    end

    private
    
    def product_includes
      [{master: [:images, :default_price]}]
    end

    def accurate_title
      if @product
        @product.meta_title.blank? ? @product.name : @product.meta_title
      else
        super
      end
    end

    def load_product
      if try_spree_current_user.try(:has_spree_role?, "admin")
        @products = Product.with_deleted
      else
        @products = Product.active(current_currency)
      end
      @product = @products.friendly.find(params[:id])
    end

    def load_taxon
      @taxon = Spree::Taxon.find(params[:taxon]) if params[:taxon].present?
    end
  end
end
