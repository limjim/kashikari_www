module Spree
  CheckoutController.class_eval do


    private

    # Provides a route to redirect after order completion
    def completion_route
      complete_order_path(:id => @order)
    end

  end
end
