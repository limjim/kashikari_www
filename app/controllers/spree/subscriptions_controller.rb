module Spree
  class SubscriptionsController < StoreController
    prepend_before_filter :check_registration, except: [:registration]
    before_action :load_object
    before_action :load_creditcard, only: [:step2]
    before_action :find_active_plan, only: [:step2, :create]
    #before_action :find_plan, only: [:destroy]
    before_action :find_subscription, only: [:destroy]
    before_action :authenticate_subscription, only: [:step2, :create]

    def step1

    end

    def step2
      @subscription = @plan.subscriptions.build
    end

    def new
      @subscription = @plan.subscriptions.build
    end

    def create
      @subscription = @plan.subscriptions.build(subscription_params.merge(user_id: spree_current_user.id))

      if @subscription.save_and_manage_api

        if subscription_params[:address_attributes].present?
        b_address = spree_current_user.bill_address || spree_current_user.build_bill_address
        b_address.attributes = subscription_params[:address_attributes]
        b_address.country = Spree::Country.find(Spree::Config[:default_country_id])
        b_address.save
        spree_current_user.update_attributes(bill_address_id: b_address.id)

        s_address = try_spree_current_user.ship_address || try_spree_current_user.build_ship_address
        s_address.attributes = subscription_params[:address_attributes]
        s_address.country = Spree::Country.find(Spree::Config[:default_country_id])
        s_address.save
        spree_current_user.update_attributes(ship_address_id: s_address.id)
        end

        @subscription.reload
        SubscriptionMailer.subscribed_email(@subscription).deliver

        case @plan.name
          when 'SILVER' then
            redirect_to thanks_silver_path, notice: "#{@plan.name}プランの購入が完了しました"
          when 'GOLD' then
            redirect_to thanks_gold_path, notice: "#{@plan.name}プランの購入が完了しました"
          when 'BLACK'
            redirect_to thanks_black_path, notice: "#{@plan.name}プランの購入が完了しました"
          else
        end
      else
        redirect_to subscriptions_step1_path, notice: "カードの決済に失敗しました。入力したカードの番号が合っているか確認してください。"
      end
    end

    def destroy
      if @user.rental_items.where(state: ["ready", "shipped"]).present?
        flash[:error] = "レンタル商品を全て返却するまで解約は出来ません"
        redirect_to account_path
        return
      end

      if @subscription.save_and_manage_api(unsubscribed_at: Time.current)
        redirect_to account_path, notice: "解約が完了しました"
      else
        redirect_to root_path
      end
    end

    private

    def find_active_plan
      unless @plan = Spree::Plan.active.where(id: params[:plan_id]).first
        flash[:error] = "有効なプランが見つかりません"
        redirect_to subscriptions_step1_path
      end
    end

    def find_plan
      unless @plan = Spree::Plan.where(id: params[:plan_id]).first
        flash[:error] = "プランが見つかりません"
        redirect_to subscriptions_step1_path
      end
    end

    def find_subscription
      unless @subscription = Spree::Subscription.find_by(id: params[:id])
        flash[:error] = "購入しているプランが見つかりません"
        redirect_to subscriptions_step1_path
      end
    end

    def subscription_params
      params.require(:subscription).permit(:email, :card_token, :existing_card, source_attributes: [:name, :gateway_payment_profile_id, :last_digits, :month, :year, :cc_type], address_attributes: [:first_name, :last_name, :address1, :address2, :city, :states, :zipcode, :phone])
    end

    def load_creditcard
      if try_spree_current_user && try_spree_current_user.respond_to?(:payment_sources)
        @payment_sources = try_spree_current_user.payment_sources
      end
    end

    def check_registration
      return if spree_current_user
      store_location
      redirect_to spree.signup_path
    end

    def load_object
      @user ||= spree_current_user
      #authorize! params[:action].to_sym, @user
    end

    def authenticate_subscription
      # Check use doesn't subscribe
      if spree_current_user.subscriptions.undeleted.any? && spree_current_user.subscriptions.undeleted.pluck(:plan_id).include?(params[:plan_id].to_i)
        flash[:error] = "該当プランはすでに購入されています"
        redirect_to subscriptions_step1_path
        return
      end

      # Check user doesn't have more than 1 subscription
      if spree_current_user.subscriptions.undeleted.any? && !spree_current_user.subscriptions.undeleted.one?
        flash[:error] = "プランを複数購入しています。サービス運営会社にお問い合わせください。"
        redirect_to subscriptions_step1_path
        return
      end

    end

    def assign_default_addresses
      if try_spree_current_user
        @bill_address = try_spree_current_user.bill_address.try(:clone) if try_spree_current_user.bill_address.try(:valid?)
        @ship_address = try_spree_current_user.ship_address.try(:clone) if try_spree_current_user.ship_address.try(:valid?)
      end
    end

  end
end