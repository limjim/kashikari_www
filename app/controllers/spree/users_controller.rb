module Spree
  class Spree::UsersController < Spree::StoreController
    ssl_required
    skip_before_filter :set_current_order, :only => :show
    prepend_before_filter :load_object, :only => [:show, :edit, :update, :thankyou, :add_to_favorite]
    prepend_before_filter :authorize_subscribed_user, :only => [:unsubscribe, :edit_address, :setting, :billing, :history, :update_address, :orders]
    prepend_before_filter :authorize_actions, :only => :new

    include Spree::Core::ControllerHelpers

    def show
      @orders = @user.orders.complete.order('completed_at desc')
      @favorites = @user.favorites.limit(12).order('created_at desc')
      @rental_items = @user.rental_items.where.not(state: ["purchased", "returned", "cancel"])
      @unavailable_rental_items = @user.rental_items.where.not(state: ["returned", "cancel"])
      @subscription = @user.subscriptions.undeleted.first
    end

    def favorites
      @user ||= spree_current_user
      @orders = @user.orders.complete.order('completed_at desc')
      @favorites = @user.favorites.order('created_at desc').page(params[:page]).per(Spree::Config[:products_per_page])
      @rental_items = @user.rental_items.where.not(state: ["purchased", "returned", "cancel"])
      @unavailable_rental_items = @user.rental_items.where.not(state: ["returned", "cancel"])
      @subscription = @user.subscriptions.undeleted.first
    end

    def edit
      @orders = @user.orders.complete.order('completed_at desc')
      @favorites = @user.favorites
      @rental_items = @user.rental_items.undeleted
    end

    def edit_address
      @orders = @user.orders.complete.order('completed_at desc')
      @favorites = @user.favorites
      @rental_items = @user.rental_items.undeleted
    end

    def unsubscribe
      @orders = @user.orders.complete.order('completed_at desc')
      @favorites = @user.favorites
      @rental_items = @user.rental_items.undeleted
      @subscription = @user.subscriptions.undeleted.first
    end

    def setting
      @orders = @user.orders.complete.order('completed_at desc')
      @favorites = @user.favorites
      @rental_items = @user.rental_items.undeleted
      @subscription = @user.subscriptions.undeleted.first
    end

    def billing
      @billings = @user.subscription_events.order("id desc")
      if @billings.where(request_type: "recursion.succeeded").any?
        latest_billing = JSON.parse(@billings.where(request_type: "recursion.succeeded").first.response)
        next_scheduled = latest_billing['data']['object']['next_scheduled']
        if next_scheduled.present?
          @next_scheduled = Time.at(next_scheduled).strftime("%Y年%m月%d日")
        end
      end
    end

    def history
      @orders = @user.orders.complete.order('completed_at desc')
      @favorites = @user.favorites
      @rental_items = @user.rental_items.where(state: ["purchased", "returned"])
    end

    def orders
      @orders = @user.orders.complete.order('completed_at desc')
      @favorites = @user.favorites
      @rental_items = @user.rental_items.where(state: ["purchased", "returned"])
    end

    def create
      @user = Spree::User.new(user_params)
      if @user.save
        if current_order
          session[:guest_token] = nil
        end
        redirect_back_or_default(root_url)
      else
        render :new
      end
    end

    def thankyou

    end

    def thanks_silver
      if !spree_current_user
        redirect_to spree.login_path
        return
      end
    end

    def thanks_gold
      if !spree_current_user
        redirect_to spree.login_path
        return
      end
    end

    def thanks_black
      if !spree_current_user
        redirect_to spree.login_path
        return
      end
    end

    def update
      if @user.update_attributes(user_params)
        if params[:user][:password].present?
          # this logic needed b/c devise wants to log us out after password changes
          user = Spree::User.reset_password_by_token(params[:user])
          sign_in(@user, :event => :authentication, :bypass => !Spree::Auth::Config[:signout_after_password_change])
        end
        redirect_to spree.account_url, :notice => Spree.t(:account_updated)
      else
        render :edit
      end
    end

    def update_address
      if user_params[:address_attributes].present?
        b_address = @user.bill_address || @user.build_bill_address
        b_address.attributes = user_params[:address_attributes]
        b_address.country = Spree::Country.find(Spree::Config[:default_country_id])
        b_address.save
        @user.update_attributes(bill_address_id: b_address.id)

        s_address = @user.ship_address || @user.build_ship_address
        s_address.attributes = user_params[:address_attributes]
        s_address.country = Spree::Country.find(Spree::Config[:default_country_id])
        s_address.save
        @user.update_attributes(ship_address_id: s_address.id)

      end

      redirect_to spree.user_setting_path, :notice => Spree.t(:account_updated)
    end

    def add_to_favorite
      # 該当Variantがあるか確認
      @variant = Variant.find(params[:variant_id])

      # Favoriteにすでにあるか確認
      @favorite = Favorite.where(user_id: @user.id, variant_id: @variant.id).first
      if @favorite
        @favorite.destroy
      else
        @favorite = Favorite.new(user_id: @user.id, variant_id: @variant.id)
        @favorite.save
      end

      if request.xhr?
        render json: { id: params[:variant_id] }
      else
        redirect_to product_path(@variant.product)
      end
    end

    private
    def user_params
      params.require(:user).permit(Spree::PermittedAttributes.user_attributes.push(address_attributes: [:first_name, :last_name, :address1, :address2, :city, :states, :zipcode, :phone]))
    end

    def load_object
      @user ||= spree_current_user
      authorize! params[:action].to_sym, @user
    end

    def load_object_for_favorite
      if !spree_current_user
        redirect_to spree.login_path
        return
      end

      @user ||= spree_current_user
    end

    def authorize_subscribed_user
      if !spree_current_user
        redirect_to spree.login_path
        return
      end

      if spree_current_user.subscriptions.undeleted.blank?
        redirect_to spree.root_path
        return
      end

      @user ||= spree_current_user
    end

    def authorize_actions
      authorize! params[:action].to_sym, Spree::User.new
    end

    def accurate_title
      Spree.t(:my_account)
    end
  end
end
