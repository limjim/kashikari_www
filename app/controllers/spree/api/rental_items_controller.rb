module Spree
  module Api
    class RentalItemsController < Spree::Api::BaseController
      def update
        @rental_item = Spree::RentalItem.accessible_by(current_ability, :update).readonly(false).find_by!(id: params[:id])
        @rental_item.update_attributes(rental_item_params)

        respond_with(@rental_item.reload, default_template: :show)
      end

      private

      def rental_item_params
        if params[:rental_item] && !params[:rental_item].empty?
          params.require(:rental_item).permit(:tracking)
        else
          {}
        end
      end

    end
  end
end
