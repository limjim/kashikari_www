module Spree
  module Admin
    class RentalItemsController < Spree::Admin::BaseController
      include RansackDateSearch
      ransack_date_searchable date_col: 'created_at'

      before_action :load_rental_item, :only => [:edit, :update, :destroy, :ship, :return, :export]

      def export
        file = @rental_item.export
        send_data file, :type => 'text/csv', :filename => "rental_#{@rental_item.id}_#{@rental_item.variant.sku}_#{Time.now.strftime('%Y%m%d')}.csv"
      end

      def index
        @search = Spree::RentalItem.ransack(params[:q])
        @rental_items = @search.result.page(params[:page]).per(15)

        respond_to do |format|
          format.html
          format.csv { render text: RentalItem.to_csv(@rental_items) }\
        end
      end

      def edit
      end

      def update

      end

      def destroy

      end

      def ship
        # Shipped Email
        @rental_item.ship!
        flash[:success] = Spree.t(:shipped)
        RentalMailer.rental_email(@rental_item).deliver
        redirect_to :back
      end

      def return
        # Returned Email
        @rental_item.return!
        flash[:success] = Spree.t(:returned)
        RentalMailer.return_email(@rental_item).deliver
        redirect_to :back
      end

      def purchase
        # Purchased Email
        @rental_item.purchase!
        flash[:success] = Spree.t(:purchased)
        redirect_to :back
      end

      private

      def load_rental_item
        @rental_item = Spree::RentalItem.find(params[:id])
      end

    end
  end
end