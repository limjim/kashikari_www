class Spree::Admin::CsvImportsController < Spree::Admin::BaseController
  respond_to :html, :js

  def index
    respond_to do |format|
      format.html { render :layout => !request.xhr? }
      format.js { render :layout => false }
    end
  end

  def template
    respond_to do |format|
      format.csv { send_data Spree::CsvImport.generate_template(params[:import_fields]), filename: 'import_template.csv', type: 'text/csv' }
    end
  end

  def import
    begin
      import_result = Spree::CsvImport.import(params[:file])
    rescue Exception => e
      @error_message = e.message
      respond_to do |format|
        format.html { render :index }
      end
    else
      respond_to do |format|
        format.html { redirect_to admin_products_path, notice: "#{import_result}件のインポートに成功しました"}
      end
    end
  end

end
