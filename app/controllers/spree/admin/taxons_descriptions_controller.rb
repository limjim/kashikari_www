module Spree
  module Admin
    class TaxonsDescriptionsController < Spree::Admin::BaseController

      before_action :find_taxons_description, :only => [:edit, :destroy, :update]
      before_action :find_taxons
      def index
    	 @taxons_descriptions = Spree::TaxonsDescription.includes([:taxon_1st, :taxon_2nd]).undeleted.order('id desc').page(params[:page]).per(10)
      end

      def new
          @taxons_description = Spree::TaxonsDescription.new
      end

      def update
        if @taxons_description.update_attributes(taxons_description_params(:update))
          flash[:notice] = "説明文が更新されました。"
          redirect_to edit_admin_taxons_description_url(@taxons_description)
        else
          render :edit
        end
      end

      def create
        @taxons_description = Spree::TaxonsDescription.new(taxons_description_params)
        if @taxons_description.save
          flash[:notice] = "説明文を追加しました。"
          redirect_to edit_admin_taxons_description_url(@taxons_description)
        else
          render :new
        end
      end

      def destroy
        @taxons_description.restrictive_destroy
      end

      private
      def taxons_description_params(action = :create)
        if action == :create
          params.require(:taxons_description).permit(:taxon_1st_id, :taxon_2nd_id, :description)
        else
          params.require(:taxons_description).permit(:description)
        end
      end

      def find_taxons_description
        unless @taxons_description = Spree::TaxonsDescription.undeleted.where(id: params[:id]).first
          flash[:error] = "説明文が見つかりません。"
          respond_to do |format|
            format.html {redirect_to admin_taxons_descriptions_url}
            format.js { }
          end
        end
      end

      def find_taxons
        brand = Taxonomy.find_by_name("brand")
        @brand_taxons = Taxon.where(parent_id: brand.id)
        item = Taxonomy.find_by_name("item")
        @item_taxons = Taxon.where(parent_id: item.id)
      end

    end
  end
end
