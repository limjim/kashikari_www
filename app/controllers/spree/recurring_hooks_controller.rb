module Spree
  class RecurringHooksController < BaseController
    skip_before_filter :verify_authenticity_token

    before_action :authenticate_webhook
    before_action :ignore_charge_event_for_subscription
    before_action :find_subscription

    respond_to :json

    def handler
      credit_card = Spree::CreditCard.find_by(gateway_customer_profile_id: event[:data][:object][:customer])
      @user = credit_card.user

      if @user.blank?
        logger.info "User is blank"
      end

      @subscription_event = @user.subscription_events.build(subscription_event_params)

      if @subscription
       @subscription_event.subscription = @subscription

        if @subscription_event.request_type == "recursion.failed"
           @subscription.save_and_manage_api(unsubscribed_at: Time.current)
        end

      end

      if @subscription_event.save
        render_status_ok
      else
        render_status_failure
      end
    end

    private

    def event
      @event ||= params.deep_dup
    end

    def authenticate_webhook
      return render_status_failure if env["HTTP_X_WEBPAY_ORIGIN_CREDENTIAL"] != Kashikari::Application.config.webpay_hook_credential

      render_status_ok if event.blank? || (!Spree::Recurring::WebpayRecurring::WEBHOOKS.include?(event[:type]))
    end

    def ignore_charge_event_for_subscription
       if event[:type] == "charge.succeeded" && event[:data][:object][:recursion].present?
         render_status_ok
       end
    end

    def find_subscription
      @subscription = Spree::Subscription.find_by(gateway_recursion_id: event[:data][:object][:id])
    end

    def subscription_event_params
      if @user
        { event_id: event[:id], request_type: event[:type], response: event.to_json }
      else
        {}
      end
    end

    def render_status_ok
      render text: '', status: 200
    end

    def render_status_failure
      render text: '', status: 403
    end
  end
end