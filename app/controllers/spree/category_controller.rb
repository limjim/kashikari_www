module Spree
  class CategoryController < StoreController
    rescue_from ActiveRecord::RecordNotFound, :with => :render_404
    helper 'spree/products'

    respond_to :html

    def show
      @taxon = Taxon.find_by_permalink!(params[:id])
      return unless @taxon

      @searcher = build_searcher(params.merge(category_id: @taxon.id, include_images: true))
      @products = @searcher.retrieve_products
      @descriptions = TaxonsDescription.where(taxon_2nd_id: @taxon.id)
    end

    private

    def accurate_title
      if @taxon
        @taxon.seo_title
      else
        super
      end
    end

  end
end