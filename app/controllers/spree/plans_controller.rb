require 'spree/core/validators/email'
module Spree
  class PlansController < StoreController
    before_filter :check_registration, :except => [:registration]

    def index
      # if spree_current_user.subscriptions.any?
      #   redirect_to plans_change_path
      # end

      @plans = Spree::Plan.visible.order('id desc')
      @subscription = Spree::Subscription.new
    end

    def step2

    end

    def change
      @current_plan = spree_current_user.subscriptions.undeleted
      @plans = Spree::Plan.visible.order('id desc')
    end

    def registration
      @user = Spree::User.new
    end

    private
    # Introduces a registration step whenever the +registration_step+ preference is true.
    def check_registration
      return if spree_current_user
      store_location
      redirect_to spree.plans_registration_path
    end

  end
end