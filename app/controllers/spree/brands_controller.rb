module Spree
  class BrandsController < StoreController
    rescue_from ActiveRecord::RecordNotFound, :with => :render_404
    helper 'spree/products'

    respond_to :html

    def index
      require 'unf'
      require 'nkf'
      @brands = Spree::Taxonomy.where(name: 'brand')
      # 半角カナの配列を生成し、全角カナに変換
      @ini_ary = ('ｱ'..'ﾜ').to_a.push('ｦ','ﾝ','その他').map { |ini| NKF.nkf('-w -Z4', ini) }
    end

    def show
      @taxon = Taxon.find_by_permalink!(params[:id])
      return unless @taxon

      @searcher = build_searcher(params.merge(brand_id: @taxon.id, include_images: true))
      @products = @searcher.retrieve_products
      @descriptions = TaxonsDescription.where( taxon_1st_id: @taxon.id )
    end

    def show_with_category

      @brand = Taxon.find_by_permalink!(params[:id])
      @category = Taxon.find_by_permalink!(params[:sub_id])

      return unless @taxon || @category
        @searcher = build_searcher(params.merge(brand_id: @brand.id, category_id: @category.id, include_images: true))
        render :json => @searcher
      @products = @searcher.retrieve_products
      # @descriptions = TaxonsDescription.undeleted.where("taxon_1st_id = ? OR taxon_2nd_id = ?", @brand.id, @category.id)
        @descriptions = TaxonsDescription.where("taxon_1st_id = ? AND taxon_2nd_id = ?", @brand.id, @category.id)
    end

    private

    def accurate_title
      if @taxon
        @taxon.seo_title
      else
        super
      end
    end

  end
end