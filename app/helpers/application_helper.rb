module ApplicationHelper
  def rental_event_links
    links = []
    ["ship", "return"].sort.each do |event|
      if @rental_item.send("can_#{event}?")
        links << button_link_to(Spree.t(event), [event, :admin, @rental_item],
                                :method => :put,
                                :icon => "#{event}",
                                :data => { :confirm => Spree.t(:order_sure_want_to, :event => Spree.t(event)) })
      end
    end
    links.join('&nbsp;').html_safe
  end

  def merge_search_params(id)
    if params[:taxon_ids].blank?
      current = []
    else
      current = params[:taxon_ids].split(",")
    end

    current.push(id.to_s).uniq!

    params.merge({:taxon_ids => current.join(",")})
  end

  def filter_controller?
    ["spree/products", "spree/brands", "spree/category"].include?(params[:controller])
  end

  def head_title(title=nil)
    title ||= Kashikari::Application.config.default_title
    title
  end

  def meta_tags(keywords=nil, description=nil)
    meta = {}
    meta[:keywords] = keywords ||=  Kashikari::Application.config.default_meta_keywords
    meta[:description] = description ||= Kashikari::Application.config.default_meta_description

    lines = meta.map do |name, content|
      tag('meta', name: name, content: content)
    end.join("\n").html_safe

    lines
  end

  def ogp_tags(image=nil, type=nil, title=nil, description=nil)

    ogp = {}

    url = request.url.to_s[-1,1] != "/" ? "#{request.url}/" : "#{request.url}"
    ogp[:"og:url"] = url
    ogp[:"og:image"] = image if image.present?
    ogp[:"og:type"] = type if type.present?
    ogp[:"og:title"] = title if title.present?
    ogp[:"og:description"] = description if description.present?

    ogp.reverse_merge!({
                           :"og:image" => asset_url("logo.gif"),
                           :"og:site_name" => Kashikari::Application.config.site_name,
                           :"og:title" => Kashikari::Application.config.default_title,
                           :"og:type" => "article",
                           :"og:description" => Kashikari::Application.config.default_meta_description,
                       })
    lines = ogp.map do |name, content|
      tag('meta', property: name, content: content)
    end.join("\n").html_safe

    lines
  end

end
