module ProductsHelper

  # プランごとの在庫合計額を取得する
  def stock_total_jpy(plan_id)
    con = ActiveRecord::Base.connection
    result = con.select_all("SELECT TRUNCATE(SUM(p.amount), 0) FROM spree_plan_products AS plan_p INNER JOIN spree_variants AS v ON plan_p.product_id = v.product_id INNER JOIN spree_prices AS p ON v.id = p.variant_id WHERE plan_id = #{plan_id};")
    number_to_currency(result.rows[0][0])
  end

  # プランごとの在庫数取得する
  def stock_of_the_plan(plan_id)
    stock_cnt = Spree::PlanProduct.includes(:product).where(spree_plan_products: {plan_id: plan_id}).where.not(spree_products: {available_on: nil }).count
    # 通貨に変更後円を点に変換して返却
    number_to_currency(stock_cnt).to_s.gsub('円','点')
  end

end
