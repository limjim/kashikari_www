module Spree::BrandsHelper

  # 分類IDを引数に商品数を取得
  def get_product_count(taxon_id)
    con = ActiveRecord::Base.connection
    result = con.select_all("SELECT count(id) FROM spree_products_taxons where taxon_id = #{taxon_id}")
    result.rows[0][0]
  end
end
