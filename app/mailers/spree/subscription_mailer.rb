module Spree
  class SubscriptionMailer < BaseMailer

    def welcome_email(user, resend = false)
      @user = user.respond_to?(:id) ? user : Spree::User.find(user)
      subject = "【KASHI KARI】無料会員登録が完了しました。"
      mail(to: @user.email, bcc: bcc_address, from: "KASHI KARI カスタマーサポート <#{from_address}>", subject: subject)
    end


    def subscribed_email(subscription, resend = false)
      @subscription = subscription.respond_to?(:id) ? subscription : Spree::Subscription.find(subscription)
      subject = "【KASHI KARI】#{@subscription.plan.name}プランの申込みが完了しました。"
      mail(to: @subscription.email, bcc: bcc_address, from: "KASHI KARI カスタマーサポート <#{from_address}>", subject: subject)
    end
  end
end
