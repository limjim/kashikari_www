module Spree
  class OrderMailer < BaseMailer
    def confirm_email(order, resend = false)
      @order = order.respond_to?(:id) ? order : Spree::Order.find(order)
      subject = "【ご注文を受け付けました】ファッションレンタル KASHI KARIをご利用ありがとうございます。"
      mail(to: @order.email, bcc: bcc_address, from: "KASHI KARI カスタマーサポート <#{from_address}>", subject: subject)
    end

    def cancel_email(order, resend = false)
      # @order = order.respond_to?(:id) ? order : Spree::Order.find(order)
      # subject = (resend ? "[#{Spree.t(:resend).upcase}] " : '')
      # subject += "#{Spree::Store.current.name} #{Spree.t('order_mailer.cancel_email.subject')} ##{@order.number}"
      # mail(to: @order.email, from: from_address, subject: subject)
    end
  end
end
