module Spree
  class RentalMailer < BaseMailer

    def rental_email(rental_item, resend = false)
      @rental_item = rental_item.respond_to?(:id) ? rental_item : Spree::RentalItem.find(rental_item)
      subject = "【商品の発送完了】ファッションレンタル KASHI KARIをご利用ありがとうございます。"
      mail(to: @rental_item.user.email, bcc: bcc_address, from: "KASHI KARI カスタマーサポート <#{from_address}>", subject: subject)
    end

    def return_email(rental_item, resend = false)
      @rental_item = rental_item.respond_to?(:id) ? rental_item : Spree::RentalItem.find(rental_item)
      subject = "【商品の返却完了（弊社にて受領致しました）】ファッションレンタル KASHI KARIをご利用ありがとうございます。"
      mail(to: @rental_item.user.email, bcc: bcc_address, from: "KASHI KARI カスタマーサポート <#{from_address}>", subject: subject)
    end

  end
end
