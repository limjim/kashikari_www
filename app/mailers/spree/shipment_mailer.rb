module Spree
  class ShipmentMailer < BaseMailer
    def shipped_email(shipment, resend = false)
      @shipment = shipment.respond_to?(:id) ? shipment : Spree::Shipment.find(shipment)
      subject = "【商品の発送完了】ファッションレンタル KASHI KARIをご利用ありがとうございます。"
      mail(to: @shipment.order.email, bcc: bcc_address, from: "KASHI KARI カスタマーサポート <#{from_address}>", subject: subject)
    end
  end
end
