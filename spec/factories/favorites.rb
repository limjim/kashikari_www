FactoryGirl.define do
  factory :favorite, class: Spree::Favorite do
    variant
    user
  end

end