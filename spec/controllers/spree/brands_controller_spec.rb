require 'spec_helper'

describe Spree::BrandsController do

  describe "GET 'show'" do
    it "returns http success" do
      get 'show'
      response.should be_success
    end
  end

  describe "GET 'show_with_category'" do
    it "returns http success" do
      get 'show_with_category'
      response.should be_success
    end
  end

end
