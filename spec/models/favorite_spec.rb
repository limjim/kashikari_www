require 'spec_helper'

RSpec.describe Spree::Favorite, type: :model  do
  describe "search methods" do

    it "should have variants" do
      expect(create(:favorite).variant).not_to be_nil
    end

    it "should have user" do
      expect(create(:favorite).variant).not_to be_nil
    end

  end
end