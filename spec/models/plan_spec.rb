require 'spec_helper'

describe Spree::Plan do
  let(:recurring) { Spree::Recurring::WebpayRecurring.create!(name: 'Test recurring', active: true) }
  let(:plan) { recurring.plans.create!(amount: 1000, interval: 'month', interval_count: 1, name: 'Test Plan', currency: 'jpy') }

  it { plan.should belong_to :recurring }
  it { plan.should have_many :subscriptions }
  it { plan.should validate_presence_of :amount }
  it { plan.should validate_presence_of :interval }
  it { plan.should validate_presence_of :interval_count }
  it { plan.should validate_presence_of :name }
  it { plan.should validate_presence_of :currency }
  it { plan.should validate_presence_of :recurring_id }
  it { plan.should have_readonly_attribute :amount }
  it { plan.should have_readonly_attribute :interval }
  it { plan.should have_readonly_attribute :currency }
  it { plan.should have_readonly_attribute :id }
  it { plan.should have_readonly_attribute :interval_count }
  it { plan.should have_readonly_attribute :recurring_id }

end